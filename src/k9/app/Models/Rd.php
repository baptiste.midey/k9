<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rd extends Model
{
    use HasFactory;

    public function entretiens()
    {
        return $this->hasMany(Entretien::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    public function user()
    {
      return $this->morphOne(User::class, 'profil');
    }

}
