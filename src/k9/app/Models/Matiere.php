<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matiere extends Model
{
    use HasFactory;

    public function promotions()
    {
      return $this->BelongsToMany(Promotion::class)->withPivot('coef');
    }

    public function childrens() 
    {
      return $this->hasMany(self::class,'matiere_id');
    }

    public function parent() 
    {
      return $this->belongsTo(self::class,'matiere_id');
    }


    public function professeur()
    {
      return $this->BelongsTo(Professeur::class);
    }

    public function note()
    {
      return $this->BelongsTo(Note::class);
    }

    public function bulletins()
    {
      return $this->BelongsToMany(Bulletin::class);
    }

}
