<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'adresse',
        'raison_sociale',
        'telephone',
        'mail',
    ];

    public function user()
    {
      return $this->morphOne(User::class, 'profil');
    }
    public function tuteurs()
    {
        return $this->hasMany(Tuteur::class);
    }

    public function offres()
    {
        return $this->hasMany(Offre::class);
    }
}
