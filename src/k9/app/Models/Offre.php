<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'description',
        'pdf',
        'entreprise_id',
    ];

    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function aptitudes()
    {
        return $this->belongsToMany(Aptitude::class);
    }

    public function apprentis()
    {
        return $this->belongsToMany(Apprenti::class);
    }

    public function tuteur()
    {
        return $this->belongsTo(Tuteur::class);
    }
    
}

   