<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entretien extends Model
{
    use HasFactory;

    protected $fillable = ['date_entretien', 'decision', 'apprenti_id', 'fiche_id', 'rd_id','promotion_id','entretien_id'];

    public function apprenti()
    {
      return $this->belongsTo(Apprenti::class);
    }

    public function rd()
    {
      return $this->belongsTo(RD::class);
  }

  public function fiche()
  {
    return $this->belongsTo(Fiche::class);
  }

  public function promotion()
  {
    return $this->belongsTo(Promotion::class);
  }
}
