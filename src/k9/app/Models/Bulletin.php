<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
    use HasFactory;

    protected $fillable = [
      'commentaire',
      'apprenti_id',
      'periode_id',
  ];

    public function apprenti()
    {
      return $this->belongsTo(Apprenti::class);
    }

    public function periode()
    {
      return $this->belongsTo(Periode::class);
    }

    public function matieres()
    {
      return $this->BelongsToMany(Matiere::class);
    }
}
