<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apprenti extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = ['age', 'date_naissance', 'code_postal', 'ville', 'adresse',
                            'telephone', 'formation_actuelle', 'diplome', 'specialite_lycee',
                            'langue_vivante_1', 'langue_vivante_2', 'option_id','promotion_id'
                          ];

    public function user()
    {
      return $this->morphOne(User::class, 'profil');
    }

    public function option()
    {
      return $this->belongsTo(Option::class);
    }

    public function promotion(){
      return $this->belongsTo(Promotion::class);
    }

    public function entretiens()
    {
      return $this->hasMany(Entretien::class);
    }

    public function offres()
    {
        return $this->belongsToMany(Offre::class);
    }
    public function bulletins()
    {
      return $this->belongsToMany(Bulletin::class);
    }
    public function notes()
    {
      return $this->belongsToMany(Note::class);
    }

    public function tuteurs()
    {
        return $this->belongsToMany(Tuteur::class)->withPivot('date_debut','date_fin');
    }
}
