<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    public function matiere()
    {
      return $this->BelongsTo(Matiere::class);
    }
    public function professeur()
    {
      return $this->BelongsTo(Professeur::class);
    }

    public function apprenti()
    {
      return $this->BelongsTo(Apprenti::class);
    }

    public function bulletin()
    {
      return $this->BelongsTo(Bulletin::class);
    }

    public function periode()
    {
      return $this->BelongsTo(Periode::class);
    }
}
