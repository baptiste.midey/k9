<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Periode extends Model
{
    use HasFactory;

    public function bulletin()
    {
      return $this->HasMany(Bulletin::class);
    }
    public function note()
    {
      return $this->HasMany(Note::class);
    }
    public function promotions()
    {
      return $this->BelongsToMany(Promotion::class);
    }
}
