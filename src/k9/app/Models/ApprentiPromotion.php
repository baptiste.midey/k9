<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApprentiPromotion extends Model
{
    use HasFactory;
    protected $fillable = ['professeur_id', 'apprenti_id', 'promotion_id', 'date_inscription'];

    public function apprenti()
    {
      return $this->belongsTo(Apprenti::class);
    }

    public function promotion()
    {
      return $this->belongsTo(Promotion::class);
    }
}
