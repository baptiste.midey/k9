<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{


    protected $fillable = ['id','rd_id', 'libelle','date_debut','date_fin'];

    public function professeurs()
    {
      return $this->belongsToMany(Professeur::class);
    }

    public function apprentis()
    {
      return $this->hasMany(Apprenti::class);
    }

    public function periodes()
    {
      return $this->BelongsToMany(Periode::class);
    }

    public function options()
    {
      return $this->BelongsToMany(Option::class);
  }

  public function matieres()
  {
    return $this->BelongsToMany(Matiere::class)->withPivot('coef');
  }

  public function rd()
  {
    return $this->BelongsTo(Rd::class);
  }

    use HasFactory;

}
//des eleves - un prof - des options 
