<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tuteur extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user() 
    { 
        return $this->morphOne(User::class, 'profil');
    }

    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function apprentis()
    {
        return $this->belongsToMany(Apprenti::class)->withPivot('date_debut','date_fin');
    }

    public function offres()
    {
        return $this->hasMany(Offre::class);
    }
}
