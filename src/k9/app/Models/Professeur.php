<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Professeur extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [];

    public function user()
    {
      return $this->morphOne(User::class, 'profil');
    }

    public function promotions()
    {
      return $this->belongsToMany(Promotion::class);
    }

    public function matieres()
    {
      return $this->hasMany(Matiere::class);
    }

    public function notes()
    {
      return $this->hasMany(Note::class);
    }

    public function bulletin()
    {
      return $this->belongToMany(bulletin::class);
    }
}
