<?php

namespace App\Observers;

use App\Models\Tuteur;

class TuteurObserver
{
    /**
     * Handle the Tuteur "created" event.
     *
     * @param  \App\Models\Tuteur  $tuteur
     * @return void
     */
    public function created(Tuteur $tuteur)
    {
        //
    }

    /**
     * Handle the Tuteur "updated" event.
     *
     * @param  \App\Models\Tuteur  $tuteur
     * @return void
     */
    public function updated(Tuteur $tuteur)
    {
        //
    }

    /**
     * Handle the Tuteur "deleted" event.
     *
     * @param  \App\Models\Tuteur  $tuteur
     * @return void
     */
    public function deleted(Tuteur $tuteur)
    {
        $tuteur->user->delete();
    }

    /**
     * Handle the Tuteur "restored" event.
     *
     * @param  \App\Models\Tuteur  $tuteur
     * @return void
     */
    public function restored(Tuteur $tuteur)
    {
        //
    }

    /**
     * Handle the Tuteur "force deleted" event.
     *
     * @param  \App\Models\Tuteur  $tuteur
     * @return void
     */
    public function forceDeleted(Tuteur $tuteur)
    {
        //
    }
}
