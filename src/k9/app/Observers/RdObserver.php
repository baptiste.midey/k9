<?php

namespace App\Observers;

use App\Models\Rd;

class RdObserver
{
    /**
     * Handle the Rd "created" event.
     *
     * @param  \App\Models\Rd  $rd
     * @return void
     */
    public function created(Rd $rd)
    {
        //
    }

    /**
     * Handle the Rd "updated" event.
     *
     * @param  \App\Models\Rd  $rd
     * @return void
     */
    public function updated(Rd $rd)
    {
        //
    }

    /**
     * Handle the Rd "deleted" event.
     *
     * @param  \App\Models\Rd  $rd
     * @return void
     */
    public function deleted(Rd $rd)
    {
        $rd->user->delete();
    }

    /**
     * Handle the Rd "restored" event.
     *
     * @param  \App\Models\Rd  $rd
     * @return void
     */
    public function restored(Rd $rd)
    {
        //
    }

    /**
     * Handle the Rd "force deleted" event.
     *
     * @param  \App\Models\Rd  $rd
     * @return void
     */
    public function forceDeleted(Rd $rd)
    {
        //
    }
}
