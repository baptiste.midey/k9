<?php

namespace App\Observers;

use App\Models\Professeur;

class ProfesseurObserver
{
    /**
     * Handle the Professeur "created" event.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return void
     */
    public function created(Professeur $professeur)
    {
        //
    }

    /**
     * Handle the Professeur "updated" event.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return void
     */
    public function updated(Professeur $professeur)
    {
        //
    }

    /**
     * Handle the Professeur "deleted" event.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return void
     */
    public function deleted(Professeur $professeur)
    {
        $professeur->user->delete();
    }

    /**
     * Handle the Professeur "restored" event.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return void
     */
    public function restored(Professeur $professeur)
    {
        //
    }

    /**
     * Handle the Professeur "force deleted" event.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return void
     */
    public function forceDeleted(Professeur $professeur)
    {
        //
    }
}
