<?php

namespace App\Observers;

use App\Models\Apprenti;

class ApprentiObserver
{
    /**
     * Handle the Apprenti "created" event.
     *
     * @param  \App\Models\Apprenti  $apprenti
     * @return void
     */
    public function created(Apprenti $apprenti)
    {
        //
    }

    /**
     * Handle the Apprenti "updated" event.
     *
     * @param  \App\Models\Apprenti  $apprenti
     * @return void
     */
    public function updated(Apprenti $apprenti)
    {
        //
    }

    /**
     * Handle the Apprenti "deleted" event.
     *
     * @param  \App\Models\Apprenti  $apprenti
     * @return void
     */
    public function deleted(Apprenti $apprenti)
    {
        $apprenti->user->delete();
    }

    /**
     * Handle the Apprenti "restored" event.
     *
     * @param  \App\Models\Apprenti  $apprenti
     * @return void
     */
    public function restored(Apprenti $apprenti)
    {
        //
    }

    /**
     * Handle the Apprenti "force deleted" event.
     *
     * @param  \App\Models\Apprenti  $apprenti
     * @return void
     */
    public function forceDeleted(Apprenti $apprenti)
    {
        //
    }
}
