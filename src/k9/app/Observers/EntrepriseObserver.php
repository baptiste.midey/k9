<?php

namespace App\Observers;

use App\Models\Entreprise;
use App\Models\User;

class EntrepriseObserver
{

    /**
     * Handle events after all transactions are committed.
     *
     * @var bool
     */
    public $afterCommit = true;

    /**
     * Handle the Entreprise "created" event.
     *
     * @param  \App\Models\Entreprise  $entreprise
     * @return void
     */
    public function created(Entreprise $entreprise)
    {
        //
    }

    /**
     * Handle the Entreprise "updated" event.
     *
     * @param  \App\Models\Entreprise  $entreprise
     * @return void
     */
    public function updated(Entreprise $entreprise)
    {
        //
    }

    /**
     * Handle the Entreprise "deleted" event.
     *
     * @param  \App\Models\Entreprise  $entreprise
     * @return void
     */
    public function deleted(Entreprise $entreprise)
    {
        $user = User::find($entreprise);
        $user->delete();
    }

    /**
     * Handle the Entreprise "restored" event.
     *
     * @param  \App\Models\Entreprise  $entreprise
     * @return void
     */
    public function restored(Entreprise $entreprise)
    {
        //
    }

    /**
     * Handle the Entreprise "force deleted" event.
     *
     * @param  \App\Models\Entreprise  $entreprise
     * @return void
     */
    public function forceDeleted(Entreprise $entreprise)
    {
        //
    }
}
