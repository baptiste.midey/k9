<?php

namespace App\Http\Controllers;

use App\Models\Apprenti;
use App\Models\Promotion;
use App\Models\Option;
use App\Models\ApprentiPromotion;
use App\Models\Entretien;
use App\Models\User;
use App\Notifications\CreateUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class ApprentiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apprentis = Apprenti::all();
        return view('apprenti.index', compact('apprentis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotions = Promotion::all();
        return view('apprenti.edit', compact('promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required|max:255',
            'prenom' => 'required|max:255',
            'email' => 'required|unique:users',
            'date_naissance' => 'required',
            'telephone' => 'numeric',
            'adresse' => 'required',
            'code_postal' => 'numeric',
            'ville' => 'required',
            'formation_actuelle' => 'required',
            'diplome' => 'required',
            'specialite_lycee' => 'required',
            'langue_vivante_1' => 'required',
            'langue_vivante_2' => 'required',
            'promotion_id' => 'required',
        ]);

        $user = User::create([
        "nom" => $request->get("nom"),
        "prenom" => $request->get("prenom"),
        "email" => $request->get("email"),
        "password"=> Hash::make(Str::random(20)),
        ]);

        $apprenti = new Apprenti;
        $apprenti->age = Carbon::parse($request->get("date_naissance"))->age;
        $apprenti->date_naissance = $request->get("date_naissance");
        $apprenti->adresse = $request->get("adresse");
        $apprenti->code_postal = $request->get("code_postal");
        $apprenti->ville = $request->get("ville");
        $apprenti->telephone = $request->get("telephone");
        $apprenti->formation_actuelle = $request->get("formation_actuelle");
        $apprenti->diplome = $request->get("diplome");
        $apprenti->specialite_lycee = $request->get("specialite_lycee");
        $apprenti->langue_vivante_1 = $request->get("langue_vivante_1");
        $apprenti->langue_vivante_2 = $request->get("langue_vivante_2");
        $apprenti->promotion_id = $request->get("promotion_id");
        $apprenti->save();

        $apprenti->user()->save($user);
        $apprenti->save();

        return redirect()->route('apprenti.index')->with('success', 'Apprenti créé');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apprenti = Apprenti::find($id);
        return view('apprenti.show', compact('apprenti'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $apprenti = Apprenti::find($id);
        $promotions = Promotion::all();
        return view('apprenti.edit', compact('apprenti', 'promotions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        // $request->validate([
        //     'nom' => 'required|max:255',
        //     'prenom' => 'required|max:255',
        //     // 'email' => Rule::requiredIf($request->input('email') != au),
        //     'password' => 'required',
        //     'date_naissance' => 'required',
        //     'telephone' => 'numeric',
        //     'adresse' => 'required',
        //     'code_postal' => 'numeric',
        //     'ville' => 'required',
        //     'formation_actuelle' => 'required',
        //     'diplome' => 'required',
        //     'specialite_lycee' => 'required',
        //     'langue_vivante_1' => 'required',
        //     'langue_vivante_2' => 'required',
        //     'promotion_id' => 'required',
        // ]);
        // dd(auth()->user()->email);

        // $apprenti = Apprenti::find($id);
        // $apprenti->age = Carbon::parse($request->get("date_naissance"))->age;
        // $apprenti->date_naissance = $request->get("date_naissance");
        // $apprenti->adresse = $request->get("adresse");
        // $apprenti->code_postal = $request->get("code_postal");
        // $apprenti->ville = $request->get("ville");
        // $apprenti->telephone = $request->get("telephone");
        // $apprenti->formation_actuelle = $request->get("formation_actuelle");
        // $apprenti->diplome = $request->get("diplome");
        // $apprenti->specialite_lycee = $request->get("specialite_lycee");
        // $apprenti->langue_vivante_1 = $request->get("langue_vivante_1");
        // $apprenti->langue_vivante_2 = $request->get("langue_vivante_2");
        // $apprenti->promotion_id = $request->get("promotion_id");
        // $apprenti->save();

        // $user = $apprenti->user;
        // $user->nom = $request->get("nom");
        // $user->prenom = $request->get("prenom");
        // $user->email = $request->get("email");
        // $user->password = bcrypt($request->get("password"));
        // $user->save();
         
        // return redirect()->route('apprenti.index')->with('success', 'Apprenti modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $apprenti = Apprenti::find($id);
            Apprenti::destroy($apprenti->id);
            return redirect()->route('apprenti.index')->with('success', 'Apprenti supprimé');
        } catch (Exception $e) {
            $msg = "";
            switch ($e->getCode()) {
            case 23000:
                $msg ="L'élément ne peut etre supprimé car il est lié à un autre élément";
            break;      
            default:
                $msg = $e->getMessage();
            break;
            }
            return redirect()->back()->withErrors($msg);
        }
    }
}
