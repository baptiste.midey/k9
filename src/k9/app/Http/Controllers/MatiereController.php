<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Matiere;
use App\Models\Professeur;
use App\Models\Promotion;

class MatiereController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matieres = Matiere::all();
        return view('matieres.index',compact('matieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $matieres = Matiere::all();
        $professeurs = Professeur::all();
        $promotions = Promotion::all();
        return view('matieres.create',compact('matieres'))->with('professeurs',$professeurs)->with('promotions',$promotions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'libelle' => 'required',
            'code' => 'required',
            'professeur_id' => 'required'
        ]);
        $matiere = new Matiere();
        $matiere->libelle = $request->input('libelle');
        $matiere->coefficient = $request->input('coefficient');
        $matiere->code = $request->input('code');
        $matiere->professeur_id = $request->input('professeur_id');
        if($request->input('matiere_id')!= null) {
            $matiere->parent()->associate(Matiere::find($request->input('matiere_id')));
        }
        $matiere->save();
        if($request->get("promotions")){
            foreach ($request->get("promotions") as $p){
                $matiere->promotions()->attach($p);
            }
        }
        return redirect()->route('matieres.index')->with('success','Matière créée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function show(Matiere $matiere)
    {
        return view('matieres.show',compact('matiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function edit(Matiere $matiere)
    {
        $matieres = Matiere::all();
        $professeurs = Professeur::all();
        $promotions = Promotion::all();
        return view('matieres.edit',compact('matiere'))->with('professeurs',$professeurs)->with('promotions',$promotions)->with('matieres',$matieres);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'libelle' => 'required',
            'code' => 'required',
            'professeur' => 'required'
        ]);
        $matiere = Matiere::find($id);
        $matiere->libelle = $request->input('libelle');
        $matiere->coefficient = $request->input('coefficient');
        $matiere->code = $request->input('code');
        $matiere->professeur_id = $request->input('professeur');
        if($request->input('matiere_id')!= null) {
            $matiere->parent()->associate(Matiere::find($request->input('matiere_id')));
        }
        $matiere->save();
        $matiere->promotions()->sync($request->get("promotions"));
        return redirect()->route('matieres.index')->with('success','Matière modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Matiere  $matiere
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matiere $matiere)
    {
        $matiere->delete();
        return redirect()->route('matieres.index')->with('success','Matière supprimée avec succès');
    }

    public function matiereProfesseur() {
        $user = auth()->user();
        $matieres = Matiere::where('professeur_id', '=',$user->profil_id)->get();
        return view('professeur_dashboard.matieres',compact('matieres'));
    }
}
