<?php

namespace App\Http\Controllers;

use App\Mail\Mailinfos;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Entreprise;
use App\Models\Entretien;
use App\Models\Tuteur;
use App\Models\Apprenti;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::latest()->paginate(5);
    
        return view('users.index',compact('user'))
            ->with('i', (request()->input('page', 1) - 1) * 4);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'nom_entreprise' => Rule::requiredIf($request->input('nom_entreprise') != null),
            'adresse_entreprise' => Rule::requiredIf($request->input('adresse_entreprise') != null),
            'raison_entreprise' => Rule::requiredIf($request->input('raison_entreprise') != null),
            'telephone_entreprise' => Rule::requiredIf($request->input('telephone_entreprise') != null),
            'telephone_tuteur' => Rule::requiredIf($request->input('telephone_tuteur') != null),
        ])->validate();
        if ($request->input('nom_entreprise') and $request->input('adresse_entreprise') and $request->input('raison_entreprise') and $request->input('telephone_entreprise') != null){

            $entreprise = new Entreprise();
            $entreprise->nom = $request->input('nom_entreprise');
            $entreprise->adresse = $request->input('adresse_entreprise');
            $entreprise->raison_sociale = $request->input('raison_entreprise');
            $entreprise->telephone = $request->input('telephone_entreprise');
            $entreprise->save();
        }

        if ($request->input('telephone_tuteur') != null) {
            $tuteur = new Tuteur();
            $tuteur->telephone = $request->input('telephone_tuteur');
            $tuteur->entreprise_id = $entreprise->id;
            $tuteur->save();
        }

        $user = User::create([
            "nom" => $request->get("nom"),
            "prenom" => $request->get("prenom"),
            "email" => $request->get("email"),
            "password" => Hash::make(Str::random(20)),
        ]);

        if ($request->input('telephone_tuteur') != null) {
            $tuteur->user()->save($user);
            $tuteur->save();
        }

        Mail::to('guinguintoto917@gmail.com')->send(new Mailinfos([
            "nom" => $request->get("nom"),
            "prenom" => $request->get("prenom"),
            "email" => $request->get("email"),
        ]));
        return redirect()->route('users.index')->with('success','Utilisateur créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        $user=User::find($id);
        $user->nom=$request->get('nom');
        $user->prenom=$request->get('prenom');
        $user->email=$request->get('email');
        $user->password=bcrypt($request->get('password'));
        $user->isAdmin=false;
        $user->save();
        return redirect()->route('users.index')->with('success','Utilisateur créé avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
    
        return redirect()->route('users.index')->with('success','Utilisateur supprimé avec succès');
    }

    public function profil(){
        $user = auth()->user();
        if ($user->profil_type == null) {
            $entretien = null;
            return view('users.profil',compact('user','entretien'));
        }else {
            $entretien = Entretien::where('apprenti_id', '=',$user->profil_id)->get();
            return view('users.profil', compact('user','entretien'));
        }
    }

    public function profilUpdate(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'nom' => Rule::requiredIf($request->input('nom') != null),
            'prenom' => Rule::requiredIf($request->input('prenom') != null),
            'email' =>  Rule::requiredIf($request->input('email') != $user->mail),
            'password' => Rule::requiredIf($request->input('password') != null),
            'nom_entr' => Rule::requiredIf($request->input('nom_entr') !=null),
            'tel_entr' => Rule::requiredIf($request->input('tel_entr') !=null),
            'raison_entr' => Rule::requiredIf($request->input('raison_entr') !=null),
            'adresse_entr' => Rule::requiredIf($request->input('adresse_entre') !=null),

        ]);
        if ($user->profil_type == "App\Models\Entreprise") {
            $user->nom = $request->get('nom_entr');
            $user->prenom = $request->get('nom_entr');
            $user->email = $request->get('email');
            $user->password = bcrypt($request->get('password'));
            $user->isAdmin = false;
            $user->save();
        } else {
            $user->nom = $request->get('nom');
            $user->prenom = $request->get('prenom');
            $user->email = $request->get('email');
            $user->password = bcrypt($request->get('password'));
            $user->isAdmin = false;
            $user->save();
        }
        if ($user->profil_type == "App\Models\Apprenti") {

            $apprenti = Apprenti::find($user->profil->id);
            $apprenti->date_naissance = $user->profil->date_naissance;
            $apprenti->adresse = $request->get("adresse");
            $apprenti->code_postal = $request->get("code_postal");
            $apprenti->ville = $request->get("ville");
            $apprenti->telephone = $request->get("telephone");
            $apprenti->save();
        }
        if ($user->profil_type == "App\Models\Entreprise") {

            $entreprise = Entreprise::find($user->profil->id);
            $entreprise->nom = $request->get('nom_entr');
            $entreprise->adresse = $request->get('adresse_entr');
            $entreprise->raison_sociale = $request->get('raison_entr');
            $entreprise->telephone = $request->get('tel_entr');
            $entreprise->save();
        }

        return redirect()->route('profil')->with('success', 'Utilisateur modifié avec succès.');
    }
}
