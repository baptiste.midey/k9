<?php

namespace App\Http\Controllers;

use App\Models\Periode;
use App\Models\Promotion;
use Illuminate\Http\Request;

class PeriodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periodes = Periode::all();
        return view('periodes.index',compact('periodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotions = Promotion::all();
        return view('periodes.create',compact('promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'annee' => 'required|integer',
            'date_debut_periode' => 'required',
            'date_fin_periode' => 'required|after:date_debut_periode',
            'active' => 'required'
        ]);
        $periode = new Periode();
        $periode->annee = $request->input('annee');
        $periode->date_debut = $request->input('date_debut_periode');
        $periode->date_fin = $request->input('date_fin_periode');
        $periode->active = $request->input('active');
        $periode->save();
        if($request->get("promotions")){
            foreach ($request->get("promotions") as $p){
                $periode->promotions()->attach($p);
            }
        }
        return redirect()->route('periodes.index')->with('success','Période créée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Periode $periode
     * @return \Illuminate\Http\Response
     */
    public function show(Periode $periode)
    {
        return view('periodes.show',compact('periode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Periode  $periode
     * @return \Illuminate\Http\Response
     */
    public function edit(Periode $periode)
    {
        $promotions = Promotion::all();
        return view('periodes.edit',compact('periode'))->with('promotions',$promotions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'annee' => 'required|integer',
            'date_debut_periode' => 'required',
            'date_fin_periode' => 'required|after:date_debut_periode',
            'active' => 'required'
        ]);
        $periode = Periode::find($id);
        $periode->annee = $request->input('annee');
        $periode->date_debut = $request->input('date_debut_periode');
        $periode->date_fin = $request->input('date_fin_periode');
        $periode->active = $request->input('active');
        $periode->save();
        $periode->promotions()->sync($request->get("promotions"));
        return redirect()->route('periodes.index')->with('success','Période modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Periode  $periode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Periode $periode)
    {
        $periode->delete();
        return redirect()->route('periodes.index')->with('success','Période supprimée avec succès');
    }

    public function promotionPeriode(Request $request){
        $id = $request->get('id');
        $periode = Periode::whereRelation('promotions', 'promotion_id','=', $id)->where('active','=',1)->get();
        return response()->json($periode);
    }
}
