<?php

namespace App\Http\Controllers;

use App\Models\Matiere;
use App\Models\Note;
use App\Models\Periode;
use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Auth::user()->profil->notes;
        return view('notes.index', compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotions = Auth::user()->profil->promotions;

        return view('notes.create', compact(['promotions']));
    }
    public function createMatieres($promotion_id)
    {
        $matieresPromotions = Promotion::find($promotion_id)->matieres;
        $matieres = [];
        $matieresResult = [];
        return response()->json($matieresPromotions);
        foreach ($matieresPromotions as $matiere) {
            if (isset($matiere->childrens)) {
                foreach ($matiere->childrens as $child) {
                    array_push($matieres, $child);
                }
            } else {
                array_push($matieres, $matiere);
            }
        }

        $matieresResult = Arr::where($matieres, function ($value, $key) {
            return $value['professeur_id'] == Auth::user()->profil->id;
        });

        return response()->json($matieresResult);
    }
    public function createApprentis($promotion_id)
    {
        return Promotion::find($promotion_id)->apprentis->load('user');
    }
    public function createPeriodes($promotion_id)
    {
        return Promotion::find($promotion_id)->periodes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'note' => 'required',
            'coefficient' => 'required',
            'eleve_id' => 'required',
            'periode_id' => 'required',
            'matiere_id' => 'required'
        ]);

        $note = new Note();
        $note->note = $request->input('note');
        $note->coef = $request->input( 'coefficient');
        $note->date = Carbon::now();
        $note->matiere_id = $request->input( 'matiere_id');
        $note->professeur_id = Auth::user()->profil->id;
        $note->apprenti_id = $request->input( 'eleve_id');
        $note->periode_id = $request->input( 'periode_id');
        $note->save();

        return redirect()->route('notes.index')->with('success', 'Note créée avec succès');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function edit(Note $note)
    {
        $promotions = Auth::user()->profil->promotions;

        $promotion_id = $note->apprenti->promotion->id;

        $matieresPromotions = Promotion::find($promotion_id)->matieres;
        $matieres = [];
        $matieresResult = [];
        foreach ($matieresPromotions as $matiere) {
            if ($matiere->childrens->isNotEmpty()) {
                foreach ($matiere->childrens as $child) {
                    array_push($matieres, $child);
                }
                dd($matiere);
            } else {
                array_push($matieres, $matiere);
            }
        }
        
        $matieresResult = Arr::where($matieres, function ($value, $key) {
            return $value['professeur_id'] == Auth::user()->profil->id;
        });
        
        $apprentis = Promotion::find($promotion_id)->apprentis->load('user');
        $periodes = Promotion::find($promotion_id)->periodes;
        
        //dd($matieresResult);

        return view('notes.edit', compact(['promotions', 'periodes','note','apprentis','matieresResult']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
        $note->delete();
        return redirect()->route('notes.index')->with('success', 'Note supprimée avec succès');
    }
}
