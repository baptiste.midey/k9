<?php

namespace App\Http\Controllers;

use App\Models\Apprenti;
use App\Models\Entreprise;
use App\Models\Tuteur;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TuteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tuteurs = Tuteur::all();
        return view('tuteurs.index', compact('tuteurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entreprises = Entreprise::all();
        $apprentis = Apprenti::all();
        return view('tuteurs.edit', compact('entreprises','apprentis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email',
            'telephone' => array('required','size:10','regex:/^((\+)33|0)[1-9](\d{2}){4}$/'),
            'entreprise_id' => 'required',
            'apprenti_id' => 'numeric|nullable',
            'apprenti_debut' => 'requiredif:apprenti_id,!=,nullable|date_format:Y-m-d|after:yesterday|nullable',
            'apprenti_fin' => 'date_format:Y-m-d|after:date_debut|nullable',
        ]);
        $tuteurData = [
            "telephone"=>$data["telephone"],
            "entreprise_id"=>$data[ "entreprise_id"]
        ];
        $t = Tuteur::create($tuteurData);
        $userData = [
            "nom" => $data["nom"],
            "prenom" => $data["prenom"],
            "email" => $data["email"],
            
            "password"=> Hash::make(Str::random(20)),
        ];
        $u = User::create($userData);
        $t->user()->save($u);
        $t->apprentis()->attach($data['apprenti_id'], ['date_debut' => $data['apprenti_debut'], 'date_fin' => $data['apprenti_fin']]);
        $t->save();

        $user = new User;
        $user->nom = $request->input('nom');
        $user->prenom = $request->input('nom');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        // $user->password = Hash::make('password', [
        //     'rounds' => 12,
        // ]);
        $user->save();

        $tuteur = new Tuteur;
        $tuteur->telephone = $request->input('telephone');
        $tuteur->entreprise = $request->input('entreprise_id');
        $tuteur->save();
        // $tuteurData = [
        //     "telephone"=>$data["telephone"],
        //     "entreprise_id"=>$data[ "entreprise_id"]
        // ];
        // $t = Tuteur::create($tuteurData);
        // $userData = [
        //     "nom" => $data["nom"],
        //     "prenom" => $data["prenom"],
        //     "email" => $data["email"],
        //     "password" => $data["password"]
        // ];
        // $u = User::create($userData);
        // $t->user()->save($u);
        // $t->apprentis()->attach($data['apprenti_id'], ['date_debut' => $data['apprenti_debut'], 'date_fin' => $data['apprenti_fin']]);
        // $t->save();

        return redirect()->route('tuteurs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tuteur = Tuteur::find($id);
        return view('tuteurs.show', compact('tuteur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tuteur = Tuteur::find($id);
        $entreprises = Entreprise::all();
        $apprentis = Apprenti::all();
        $tuteurApprentis = $tuteur->apprentis->sortby('date_debut');
        $tuteurApprentis = $tuteurApprentis->all();
        return view('tuteurs.edit', compact('entreprises','tuteur','apprentis', 'tuteurApprentis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email',
            'telephone' => array('required', 'size:10', 'regex:/^((\+)33|0)[1-9](\d{2}){4}$/'),
            'entreprise_id' => 'required',
            'apprenti_id' => 'numeric|nullable',
            'apprenti_debut' => 'requiredif:apprenti_id,!=,null|date|nullable',
            'apprenti_fin' => 'date|nullable',
        ]);
        $t = Tuteur::find($id);
        $t->user->nom = $data["nom"];
        $t->user->prenom = $data["prenom"];
        $t->user->email = $data["email"];
        $t->telephone = $data["telephone"];
        $t->entreprise_id = $data["entreprise_id"];
        $t->apprentis()->attach($data['apprenti_id'],['date_debut'=>$data['apprenti_debut'], 'date_fin' => $data['apprenti_fin']]);
        $t->save();

        return redirect()->route('tuteurs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tuteur = Tuteur::find($id);
        $tuteur->delete();
        return redirect()->route('tuteurs.index');
    }
}
