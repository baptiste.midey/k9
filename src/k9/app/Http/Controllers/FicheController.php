<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{Fiche,Entretien};
use PDF;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class FicheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fiches = Fiche::all();
        return view('fiches.index', compact('fiches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entretiens = Entretien::all();
        return view('fiches.create',compact('entretiens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'demandeur_emploi'      =>  'required',
            'autre_demandeur'       =>  Rule::requiredIf($request->input('autre_demandeur') != null),
            'formation_projet'      =>  'required',
            'connaissance'          =>  'required',
            'motivation'            =>  'required',
            'niveau_scolaire'       =>  'required',
            'experience_pro'        =>  'required',
            'experience_pro_text'   =>  'required',
            'atouts'                =>  'required',
            'difficultes_solutions' =>  'required',
            'entreprise_trouvee'    =>  'required',
            'post_conforme'         =>  'required',
            'demarche'              =>  'required',
            'type_entreprise'       =>  'required',
            'secteur_geo'           =>  'required',
            'bilan'                 =>  'required',
            'amenagements'          =>  'required',
            'permis'                =>  'required',
            'vehicule'              =>  'required',
            'decision'              =>  'required',
            'decision2'             =>  'required',
            'entretien_id'          =>  'required'
        ]);
        $fiche = Fiche::create($data);
        $entretien = Entretien::find($request->input('entretien_id'));
        $entretien->fiche_id = $fiche->id;
        $entretien->decision = $request->input('decision2');
        $entretien->save();
        return redirect()->route('entretiens.index')->with('success', 'Votre fiche à bien été enregistrée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fiche = Fiche::Find($id);
        return json_encode($fiche);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fiche = Fiche::Find($id);
        return view('fiches.edit',compact('fiche'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fiche = Fiche::Find($id);
        $fiche->demandeur_emploi        =   $request->demandeur_emploi;
        $fiche->autre_demandeur         =   $request->autre_demandeur;
        $fiche->formation_projet        =   $request->formation_projet;
        $fiche->connaissance            =   $request->connaissance;
        $fiche->motivation              =   $request->motivation;
        $fiche->niveau_scolaire         =   $request->niveau_scolaire;
        $fiche->experience_pro          =   $request->experience_pro;
        $fiche->experience_pro_text     =   $request->experience_pro_text;
        $fiche->atouts                  =   $request->atouts;
        $fiche->difficultes_solutions   =   $request->difficultes_solutions;
        $fiche->entreprise_trouvee      =   $request->entreprise_trouvee;
        $fiche->post_conforme           =   $request->post_conforme;
        $fiche->demarche                =   $request->demarche;
        $fiche->type_entreprise         =   $request->type_entreprise;
        $fiche->secteur_geo             =   $request->secteur_geo;
        $fiche->bilan                   =   $request->bilan;
        $fiche->amenagements            =   $request->amenagements;
        $fiche->permis                  =   $request->permis;
        $fiche->vehicule                =   $request->vehicule;
        $fiche->decision                =   $request->decision;
        $fiche->save();

        return redirect()->route('fiches.index')->with('success', 'Votre fiche à bien été enregistrée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $fiche = Fiche::find($id);
        // $entretien = Entretien::find($fiche->entretien_id);
        // $entretien->fiche_id = null;
        // $entretien->save();
        // $fiche->delete();
        // return redirect()->route('fiches.index');

    }
    public function getPostPdf($id)
    {
        $fiche = Fiche::Find($id);
        view()->share('fiche',$fiche);
        $pdf = PDF::loadView('fiches.pdf',$fiche);
        return $pdf->download('fiche_entretien_'.$fiche->entretien->apprenti->user->nom.'.pdf');
    }

}
