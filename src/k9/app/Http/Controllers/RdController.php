<?php

namespace App\Http\Controllers;

use App\Models\{Promotion, Rd, User};
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rd = Rd::all();
        return view ('rd.list', compact('rd'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('rd.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    //création d'un rd
        $rd = new Rd;
        $rd->save();
        //creation de User

        $user = User::create([
            "nom" => $request->get("nom"),
            "prenom" => $request->get("prenom"),
            "email" => $request->get("email"),
            "password" => Hash::make(Str::random(20)),
        ]);

        $rd->user()->save($user);
        $rd->save();

        return redirect()->route('rd.index')->with('success', 'Responsable de dispositif créé');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rd  $rd
     * @return \Illuminate\Http\Response
     */
    public function show(Rd $rd)
    {
        return view('rd.show', compact('rd'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Rd $rd)
    {
        $promotions = Promotion::all();
        return view('rd.edit', compact('rd','promotions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rd $rd)
    {
        $user = User::find($rd->user->id);
        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->email = $request->get("email");
        $user->profil_type = "App\Models\Rd";
        $user->profil_id = $rd->id;
        $user->isAdmin = 0;
        $user->password = hash::make("password");

        foreach ($request->get("promotions") as $p){
            $promotion = Promotion::find($p);        
            $rd->promotions()->save($promotion);
        }
        $user->save();

      return redirect()->route('rd.index')->with('success', 'Responsable de dispositif modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rd $rd)
    {
        try {
            Rd::destroy($rd->id);
            return redirect()->route("rd.index")->with('success', 'Responsable de dispositif supprimé');
        } catch (Exception $e) {
            $msg = "";
            switch ($e->getCode()) {
            case 23000:
                $msg ="L'élément ne peut etre supprimé car il est lié à un autre élément";
            break;      
            default:
                $msg = $e->getMessage();
            break;
            }
            return redirect()->back()->withErrors($msg);
        }
    }
}

