<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\{Promotion, Professeur, Rd,Apprenti,Matiere};
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::get()->all();
        return view('promotions.index', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rds = Rd::all();
        return view('promotions.edit', compact('rds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = request()->validate([
        'libelle'=>'required',
        'date_debut' => 'required',
        'date_fin' => 'required|after:date_debut',
        'rd_id'=>'required',
      ]);
        Promotion::create($data);
        return redirect()->route('promotions.index')->with('success', 'Promotion créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
      return view('promotions.show', compact('promotion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
      $rds = Rd::all();
      return view('promotions.edit', compact('rds', 'promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
      $request->validate([
        'libelle' => 'required',
        'date_debut' => 'required',
        'date_fin' => 'required|after:date_debut',
        'rd_id' => 'required',
      ]);
      $promotion->update($request->all());
      return redirect()->route('promotions.index')->with('success', 'Promotion modifiée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        $promotion = Promotion::find($id);
        $promotion->delete();
        return redirect()->route('promotions.index')->with('success', 'Promotion supprimée');
      } catch (Exception $e){
        $msg = "";
          switch ($e->getCode()) {
            case 23000:
                $msg ="l'élément ne peut etre supprimés car il est lié à un autre élément";
                break;
            default:
                $msg = $e->getMessage();
                break;
          }
          return redirect()->back()->withErrors($msg);
      }
    }

    public function promotionProfesseur() {
      $user = auth()->user();
      $promotions = Promotion::whereRelation('professeurs', 'professeur_id','=', $user->profil_id)->get();
      return view('professeur_dashboard.promotions', compact('promotions'));
    }

    public function promotionApprenti(Request $request) {
      $id = $request->get('id');
      $apprentis = Apprenti::with('user')->where('promotion_id','=',$id)->get();
      return response()->json($apprentis);
    }

    public function promotionMatiere(Request $request) {
      $id = $request->get('id');
      $matieres = Matiere::whereRelation('promotions', 'promotion_id','=', $id)->get();
      return response()->json($matieres);
    }

}
