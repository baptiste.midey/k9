<?php

namespace App\Http\Controllers;

use App\Models\Offre;
use App\Models\Tuteur;
use App\Models\Aptitude;
use App\Models\Entreprise;
use App\Models\Entretien;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offre = Offre::latest()->paginate(5);
        $tuteur = Tuteur::All();
        return view('offres.index',compact('offre'))
            ->with('tuteur', $tuteur);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tuteurs = Tuteur::All();
        $aptitudes = Aptitude::All();
        $entreprises = Entreprise::All();
        return view('offres.create')->with('aptitudes', $aptitudes)->with('entreprises', $entreprises)->with('tuteurs', $tuteurs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
            'pdf' => Rule::requiredIf($request->file('pdf') != null),
            'entreprise_id' => 'required'
        ]);
        $offre = new Offre();
        $offre->nom=$request->input('nom');
        $offre->description=$request->input('description');
        $offre->entreprise_id = $request->input('entreprise_id');
        if($request->input('tuteur')!= null) {
            $offre->tuteur_id = $request->input('tuteur');
        }
        if ($request->file('pdf')) {
            $offre->pdf = $request->file('pdf')->getClientOriginalName();
            Storage::disk('pdf')->put($offre->pdf,file_get_contents($request->pdf));
        }
        $offre->save();
        foreach ($request->get("aptitudes") as $a){
            $offre->aptitudes()->attach($a);
        }
        return redirect()->route('offres.index')
                        ->with('success','Offre créée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function show(Offre $offre)
    {
        return view('offres.show',compact('offre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function edit(Offre $offre)
    {
        $tuteur = Tuteur::All();
        $aptitude = Aptitude::All();
        $entreprises = Entreprise::All();
        return view('offres.edit',compact('offre'))->with('offre', $offre)->with('tuteur', $tuteur)->with('aptitudes', $aptitude)->with('entreprises', $entreprises);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
            'pdf' => Rule::requiredIf($request->file('pdf') != null),
            'entreprise_id' => 'required'
        ]);
        
        $offre = Offre::find($id);
        $offre->nom=$request->get('nom');
        $offre->description=$request->get('description');
        if($request->input('tuteur')!= null) {
            $offre->tuteur_id = $request->input('tuteur');
        }
        $offre->entreprise_id = $request->input('entreprise_id');
        if ($request->file('pdf')) {
            $offre->pdf = $request->file('pdf')->getClientOriginalName();
            Storage::disk('pdf')->put($offre->pdf,file_get_contents($request->pdf));
        }
        $offre->aptitudes()->sync($request->get("aptitudes"));
        $offre->save();
        return redirect()->route('offres.index')
                        ->with('success','Offre modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offre $offre)
    {
        $offre->delete();
        return redirect()->route('offres.index')->with('success','Offre supprimé avec succès');
    }

    public function tuteurEntreprise(Request $request){
        $id = $request->get('id');
        $tuteur = Tuteur::with('user')->where('entreprise_id', '=',$id)->get();
        return response()->json($tuteur);
    }

    public function welcome(){
        $offre = Offre::All();
        $user = auth()->user();
        //dd($user);
        if ($user->profil_type == null) {
            $entretien = null;
            return view('welcome',compact('offre','entretien','user'));
        }else {
            $entretien = Entretien::where('apprenti_id', '=',$user->profil_id)->get();
            return view('welcome',compact('offre','entretien','user'));
        }
    }
}
