<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offre;
use App\Models\Aptitude;
use App\Models\Entreprise;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class OffreEntreprise extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $offres = Offre::where('entreprise_id', '=',$user->profil_id)->get();
        return view('offre_entreprise.index')->with('offres',$offres);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $aptitudes = Aptitude::All();
        $entreprises = Entreprise::All();
        return view('offre_entreprise.create')->with('aptitudes', $aptitudes)->with('entreprises', $entreprises);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
            'pdf' => Rule::requiredIf($request->file('pdf') != null),
        ]);
        $offre = new Offre();
        $offre->nom=$request->input('nom');
        $offre->description=$request->input('description');
        $offre->entreprise_id = $user->profil->id;
        if ($request->file('pdf')) {
            $offre->pdf = $request->file('pdf')->getClientOriginalName();
            Storage::disk('pdf')->put($offre->pdf,file_get_contents($request->pdf));
        }
        $offre->save();
        foreach ($request->get("aptitudes") as $a){
            $offre->aptitudes()->attach($a);
        }
        return redirect()->route('offre_entreprise.index')
                        ->with('success','Offre créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offre = Offre::find($id);
        return view('offre_entreprise.show')->with('offre',$offre);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offre = Offre::find($id);
        $aptitude = Aptitude::All();
        $entreprises = Entreprise::All();
        return view('offre_entreprise.edit')->with('offre', $offre)->with('aptitudes', $aptitude)->with('entreprises', $entreprises);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
            'pdf' => Rule::requiredIf($request->file('pdf') != null),
        ]);
        
        $offre = Offre::find($id);
        $offre->nom=$request->get('nom');
        $offre->description=$request->get('description');
        // $offre->tuteur_id=$request->get('tuteur');
        $offre->entreprise_id = $user->profil->id;
        if ($request->file('pdf')) {
            $offre->pdf = $request->file('pdf')->getClientOriginalName();
            Storage::disk('pdf')->put($offre->pdf,file_get_contents($request->pdf));
        }
        $offre->aptitudes()->sync($request->get("aptitudes"));
        $offre->save();
        return redirect()->route('offres.index')
                        ->with('success','Offre modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offre = Offre::find($id);
        $offre->delete();
        return redirect()->route('offre_entreprise.index')->with('success','Offre supprimé avec succès');
    }
}
