<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bulletin;
use App\Models\Promotion;
use App\Models\Apprenti;
use App\Models\Matiere;

class BulletinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bulletins = Bulletin::all();
        return view('bulletins/index', compact('bulletins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotions = Promotion::all();
        return view('bulletins/create',compact('promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bulletin = new Bulletin();
        $bulletin->periode_id = $request->input('periode');
        $bulletin->save();
        foreach ($request->get("matieres") as $m){
            $bulletin->matieres()->attach($m);
        }
        // return redirect()->route('bulletins.index')->with('success','Bulletin créé avec succès');
    //     return response()->json([
    //         'view' => view('bulletins.index', compact('bulletins'))->render(),
    //    ]);
        // return response()->json(['success'=>'Successfully']);
        return response()->json(['url'=>url('/admin/bulletins')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bulletinPromotion()
    {
        $promotions = Promotion::all();
        return view('bulletins/createAll',compact('promotions'));
    }

    public function createPromoBulletin(Request $request) {
        $id = $request->get('promotion_id');
        $apprentis = Apprenti::with('user')->where('promotion_id','=',$id)->get();
        foreach($apprentis as $a) {
            $bulletin = new Bulletin();
            $bulletin->apprenti_id = $a->id;
            $bulletin->periode_id = $request->input('periode');
            $bulletin->save();
        }
        return redirect()->route('bulletins.index')->with('success','Bulletin créé avec succès');
    }

    public function bulletinMatiere(Request $request) 
    {
        $id = $request->get('id');
        $matiere = Matiere::find($id);
        // $bulletin = new Bulletin();
        // $bulletin->save();
        // $bulletin->matieres->attach($matiere);
        return response()->json($matiere);
    }
}
