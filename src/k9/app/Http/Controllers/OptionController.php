<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Models\Promotion;
use Illuminate\Http\Request;
use Exception;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $options = Option::All();
        return view('options.index', compact('options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotions = Promotion::All();
        return view('options.create', compact('promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $option = new Option;
        $option-> libelle = $request->libelle;
        $option-> promotion_id = $request->promotion_id;
        $option->save();
        return  redirect()->route('options.index')->with('success', 'Option créée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Option $option)
    {
        return view('options.show', compact('option'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $option = Option::Find($id);
        $promotions = Promotion::All();
        return view('options.edit', compact('option','promotions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $option = Option::Find($id);
        $option-> libelle = $request->libelle;
        $option-> promotion_id = $request->promotion_id;
        $option->save();

        return  redirect()->route('options.index')->with('success', 'Option mise à jour');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Option::Find($id)->delete();
            return redirect()->route('options.index')->with('success', 'Option supprimée');;
        } catch (Exception $e) {
            $msg = "";
            switch ($e->getCode()) {
                case 23000:
                    $msg ="L'élément ne peut etre supprimé car il est lié à un autre élément";
                    break;
                
                default:
                    $msg = $e->getMessage();
                    break;
            }
            return redirect()->back()->withErrors($msg);
        }
    }
}
