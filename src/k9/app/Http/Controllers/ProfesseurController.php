<?php

namespace App\Http\Controllers;

use App\Models\{Professeur, User,Promotion};
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfesseurController extends Controller
{


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professeurs = Professeur::all();
        return view ('professeur/list', compact('professeurs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promotions = Promotion::all();
        return view ('professeur.create',compact('promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'email',
        ]);

    //création de professeur
        $professeur = new Professeur;
        $professeur->save();
        foreach ($request->get("promotions") as $p){
            $professeur->promotions()->attach($p);
        }
        //creation de User

        $user = User::create([
            "nom" => $request->get("nom"),
            "prenom" => $request->get("prenom"),
            "email" => $request->get("email"),
            "password" => Hash::make(Str::random(20)),
        ]);

        $professeur->user()->save($user);
        $professeur->save();

        return redirect()->route('professeur.index')->with('success', 'Professeur créé');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return \Illuminate\Http\Response
     */
    public function show(Professeur $professeur )
    {
        return view('professeur.show', compact('professeur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Professeur $professeur)
    {
        $promotions = Promotion::all();
        return view('professeur.edit', compact('professeur','promotions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Professeur $professeur)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'email',
            'password' => 'password',
        ]);
        $professeur->promotions()->sync($request->get("promotions"));
        $professeur->save();

        $user = User::find($professeur->user->id);
        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->email = $request->get("email");
        $user->save();

      return redirect()->route('professeur.index')->with('success', 'Professeur modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Professeur $professeur)
    {
        try {
            User::destroy($professeur->user->id);
            return redirect()->route("professeur.index")->with('success', 'Professeur supprimé');
        } catch (Exception $e) {
            $msg = "";
            switch ($e->getCode()) {
            case 23000:
                $msg ="L'élément ne peut etre supprimé car il est lié à un autre élément";
            break;      
            default:
                $msg = $e->getMessage();
            break;
            }
            return redirect()->back()->withErrors($msg);
        }
    }


    
}
