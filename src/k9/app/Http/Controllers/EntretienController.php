<?php

namespace App\Http\Controllers;

use App\Models\{Entretien, Apprenti, Professeur, ElevePromotion, Promotion, Rd,Fiche};
use Exception;
use Illuminate\Http\Request;


class EntretienController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $entretiens = Entretien::all();
    return view('entretiens/index', compact('entretiens'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $rds = Rd::all();
    $apprentis = Apprenti::all();
    $promotions = Promotion::all();
    return view('entretiens/create', compact('rds', 'apprentis', 'promotions'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = request()->validate([
      'date_entretien' => 'required',
      'decision' => 'required',
      'apprenti_id' => 'required',
      'rd_id' => 'required',
      'promotion_id' => 'required',
    ]);
    Entretien::create($data);
    return redirect()->route('entretiens.index')->with('success', 'Entretien créé');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Entretien  $entretien
   * @return \Illuminate\Http\Response
   */
  public function show(Entretien $entretien)
  {
    return view('entretiens.show', compact('entretien'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Entretien  $entretien
   * @return \Illuminate\Http\Response
   */
  public function edit(Entretien $entretien)
  {
    $rds = Rd::all();
    $apprentis = Apprenti::all();
    $promotions = Promotion::all();
    return view('entretiens/edit', compact('rds', 'apprentis', 'promotions','entretien'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Entretien  $entretien
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Entretien $entretien)
  {
    $data = $request->validate([
      'date_entretien' => 'required',
      'decision' => 'required',
      'apprenti_id' => 'required',
      'rd_id' => 'required',
      'promotion_id' => 'required',
    ]);
    $entretien->update($data);
    return redirect()->route('entretiens.index')->with('success', 'Entretien modifié');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Entretien  $entretien
   * @return \Illuminate\Http\Response
   */
  public function destroy(Entretien $entretien)
  {
    try {
      $entretien->delete();
      return redirect()->route('entretiens.index')->with('success', 'Entretien supprimé');
    } catch (Exception $e) {
      $msg = "";
      switch ($e->getCode()) {
        case 23000:
            $msg ="L'élément ne peut etre supprimé car il est lié à un autre élément";
            break;      
        default:
            $msg = $e->getMessage();
            break;
      }
      return redirect()->back()->withErrors($msg);
    }
  }
}
