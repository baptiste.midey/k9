<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class CheckIfFirstLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //return $next($request);
        $user = Auth::user();
        if (Auth::check() && $user && $user->password_changed_at == null) {
            $token = Password::createToken($user);
            $db_token = DB::table('password_resets')->where('email', $user->email)->value('token');
            if (Hash::check($token, $db_token)) {
                $redirect = redirect()->route("password.reset", [
                    "token" => $token,
                    "email" => $user->email,
                ]);
                return $redirect;

                // $url = url(route('password.reset', [
                //         'token' => $token,
                //         'email' => $user->email,
                //     ]));

                // return redirect($url);
            }
        }
        return $next($request);
    }
}
