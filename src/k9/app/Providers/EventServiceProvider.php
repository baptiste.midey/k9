<?php

namespace App\Providers;

use App\Models\Entreprise;
use App\Observers\EntrepriseObserver;
use App\Listeners\OnPasswordChanged;
use App\Models\Apprenti;
use App\Models\Professeur;
use App\Models\Rd;
use App\Models\Tuteur;
use App\Models\User;
use App\Observers\ApprentiObserver;
use App\Observers\ProfesseurObserver;
use App\Observers\RdObserver;
use App\Observers\TuteurObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        PasswordReset::class => [
            OnPasswordChanged::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Apprenti::observe(ApprentiObserver::class);
        Professeur::observe(ProfesseurObserver::class);
        Rd::observe(RdObserver::class);
        Tuteur::observe(TuteurObserver::class);
        Entreprise::observe(EntrepriseObserver::class);
    }
}
