<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'L\'attribut :attribute doit être accepté.',
    'active_url' => 'L\' :attribute n\'est pas une URL valide.',
    'after' => 'L\' :attribute doit être une date après :date.',
    'after_or_equal' => 'L\' :attribute doit être une date après ou égale à :date.',
    'alpha' => ':attribute doit contenir seulement des lettres.',
    'alpha_dash' => ':attribute doit contenir seulement des lettres, des nombres, des tirets et des underscores.',
    'alpha_num' => ':attribute doit seulement contenir des lettres et des nombres.',
    'array' => ':attribute doit être un tableau.',
    'before' => ':attribute doit être une date avant :date.',
    'before_or_equal' => ':attribute doit être une date avant ou égale to :date.',
    'between' => [
        'numeric' => ':attribute doit être entre :min et :max.',
        'file' => ':attribute  doit être entre :min et :max kilobytes.',
        'string' => ':attribute  doit être entre :min et :max caractères.',
        'array' => ' :attribute doit avoir entre :min et :max éléments.',
    ],
    'boolean' => 'Le champ :attribute doit être vrai ou faux.',
    'confirmed' => 'La confirmation de :attribute ne correspond pas.',
    'current_password' => 'Le mot de passe est incorrect.',
    'date' => 'La :attribute n\'est pas une date valide.',
    'date_equals' => ':attribute doit être une date égale à :date.',
    'date_format' => ':attribute ne correspond pas au format :format.',
    'different' => ':attribute et :other doivent être différent.',
    'digits' => ':attribute doit être :digits digits.',
    'digits_between' => ':attribute doit être entre :min et :max digits.',
    'dimensions' => ':attribute a des dimensions d\'images invalides.',
    'distinct' => 'Le champ :attribute a une valeur dupliquée.',
    'email' => 'L\':attribute doit être une adresse mail valide.',
    'ends_with' => ':attribute doit finir avec l\'un des éléments suivants : :values.',
    'exists' => 'L\' :attribute choisi est invalide.',
    'file' => 'Le :attribute doit être un fichier.',
    'filled' => 'Le champ :attribute doit avoir une valeur.',
    'gt' => [
        'numeric' => ':attribute doit être plus grand que :value.',
        'file' => ':attribute doit être plus grand que :value kilobytes.',
        'string' => ':attribute doit être plus grand que :value caractères.',
        'array' => ':attribute doit avoir plus d\'éléments que :value.',
    ],
    'gte' => [
        'numeric' => ':attribute doit être plus grand que ou équivalent à :value.',
        'file' => ':attribute doit être plus grand que ou équivalent àl :value kilobytes.',
        'string' => 'The :attribute doit être plus grand que ou équivalent à :value caractères.',
        'array' => ':attribute doit avoir un ou plusieurs éléments :value.',
    ],
    'image' => 'L\' :attribute doit être une  image.',
    'in' => 'L\' :attribute sélectionné est invalide.',
    'in_array' => 'Le champ :attribute n\'existe pas dans :other.',
    'integer' => 'Le :attribute doit être un entier.',
    'ip' => 'L\':attribute doit être une adresse IP valide.',
    'ipv4' => 'L\':attribute doit être une adresse IPv4 valide.',
    'ipv6' => 'L\':attribute doit être une adresse IPv6 valide.',
    'json' => 'L\':attribute doit être une chaîne JSON valide.',
    'lt' => [
        'numeric' => 'L\':attribute doit être plus petit que :value.',
        'file' => 'Le :attribute doit être plus petit que  :value kilobytes.',
        'string' => 'Le  :attribute doit être plus petit que :value caractères.',
        'array' => 'Le :attribute doit avoir moins d\'éléments que :value.',
    ],
    'lte' => [
        'numeric' => 'L\' :attribute doit être plus petit ou égal à :value.',
        'file' => 'Le :attribute doit être plus petit ou égal à :value kilobytes.',
        'string' => 'Le :attribute doit être plus petit ou égal à :value caractères.',
        'array' => 'Le :attribute ne doit pas avoir moins d\'éléments que :value.',
    ],
    'max' => [
        'numeric' => 'L\' :attribute ne doit pas être plus grand que :max.',
        'file' => 'Le :attribute ne doit pas être plus grand que :max kilobytes.',
        'string' => 'Le :attribute ne doit pas être plus grand que :max caractères.',
        'array' => 'Le :attribute ne doit pas avoir plus d\'éléments que :max.',
    ],
    'mimes' => 'L\' :attribute doit être une fichier du type: :values.',
    'mimetypes' => 'L\' :attribute doit être une fichier du type: :values.',
    'min' => [
        'numeric' => 'L\' :attribute doit être au moins égal à :min.',
        'file' => 'Le :attribute  doit être au moins égal à :min kilobytes.',
        'string' => 'Le :attribute  doit être au moins égal à :min caractères.',
        'array' => 'Le :attribute doit avoir au moins :min éléments.',
    ],
    'multiple_of' => 'L\' :attribute doit être un multiple de :value.',
    'not_in' => 'L\':attribute sélectionné est invalide.',
    'not_regex' => 'Le format de :attribute est invalide.',
    'numeric' => ':attribute doit être un nombre.',
    'password' => 'Le mot de passe est incorrect.',
    'present' => 'Le champ :attribute doit être présent.',
    'regex' => 'Le format de :attribute est invalide.',
    'required' => 'Le champ :attribute est requis.',
    'required_if' => 'Le champ :attribute est requis quand :other et :value.',
    'required_unless' => 'Le champ :attribute est requis sauf si :other est dans :values.',
    'required_with' => 'Le champ :attribute  est requis quand :values est présent.',
    'required_with_all' => 'Le champ :attribute est requis quand :values sont présentes.',
    'required_without' => 'Le champ :attribute est requis quand :values n\'est pas présent.',
    'required_without_all' => 'Le champ :attribute est requis quand aucune des :values ne sont présentes.',
    'prohibited' => 'Le champ :attribute est interdit.',
    'prohibited_if' => 'Le champ :attribute est interdit quand :other est :value.',
    'prohibited_unless' => 'Le champ :attribute est interdit sauf si :other est dans :values.',
    'same' => ':attribute et :other doivent correspondre.',
    'size' => [
        'numeric' => ':attribute doit être :size.',
        'file' => 'Le :attribute doit être :size kilobytes.',
        'string' => 'Le :attribute doit avoir :size caractères.',
        'array' => 'L\':attribute doit contenir :size éléments.',
    ],
    'starts_with' => ':attribute doit commencer avec une des suivantes: :values.',
    'string' => 'attribute doit être une chaîne.',
    'timezone' => ':attribute doit être une zone valide.',
    'unique' => ':attribute a déjà été pris.',
    'uploaded' => ':attribute a échoué lors du téléchargement.',
    'url' => 'Le format de :attribute est invalide.',
    'uuid' => ':attribute doit avoir une UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'date_fin' => [
            'after' => 'La date de fin de la promotion doit être supérieure à la date du début de la promotion',
        ],
        'date_fin_periode' => [
            'after' => 'La date de fin de la période doit être supérieure à la date du début de la période',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
