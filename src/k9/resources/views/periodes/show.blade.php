@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Période</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('periodes.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Année :</a>
                {{ $periode->annee }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Date de début de la période :</a>
                {{ $periode->date_debut }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Date de fin de la période :</a>
                {{ $periode->date_fin }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Période active :</a>
                @if ($periode->active =="1")
                    Oui
                @else
                    Non
                @endif
            </div>
        </div>
    </div>
@endsection     