@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter une nouvelle Période</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('periodes.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('periodes.store') }}" method="POST" enctype="multipart/form-data">
   @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Annee :</a>
                    <input type="text" name="annee" class="form-control" placeholder="Entrez un nom">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Date début :</a>
                    <input type="date" name="date_debut_periode" class="form-control" value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Date Fin :</a>
                    <input type="date" name="date_fin_periode" class="form-control" value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Periode active :</a>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="1" name="active">
                        <label class="form-check-label">Oui</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="0" name="active">
                        <label class="form-check-label">Non</label>
                    </div>    
                </div>
            </div>
            <div class="col-12">
                <a class="font-bold">Promotion : </a>
                @foreach($promotions as $promotion)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{$promotion->id}}" name="promotions[]">
                    <label class="form-check-label" for="defaultCheck1">
                        {{$promotion->libelle}}
                    </label>
                </div>
                @endforeach
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
        </div>
    </div>
</form>
@endsection