@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Periodes</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success"  href="{{ route('periodes.create') }}">Créer une période</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Année</th>
                <th>Date début</th>
                <th>Date Fin</th>
                <th>Active</th>
                <th>Promotions</th>
                <th width="220px">Action</th>
            </thead>
            <tbody>
                @foreach ($periodes as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->annee }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->date_debut)->format('d/m/Y')}}</td>
                    <td>{{ \Carbon\Carbon::parse($p->date_fin)->format('d/m/Y')}}</td>
                    @if($p->active == "1")
                        <td>Oui</td>
                    @else
                        <td>Non</td>
                    @endif
                    <td>
                        @foreach($p->promotions as $promotion)
                            {{ $promotion->libelle }},
                        @endforeach
                    </td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('periodes.edit',$p->id) }}"></a>   
                        <a class="btn btn-show col" href="{{ route('periodes.show',$p->id) }}"></a>    
                        <form action="{{ route('periodes.destroy',$p->id) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <button type="submit" class="btn btn-delete"></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>  
    </div>    
@endsection