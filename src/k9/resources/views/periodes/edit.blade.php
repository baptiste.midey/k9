@extends('backend.template')
   
@section('title')
    Modifier la période
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modification de la période</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('periodes.index') }}">Retour </a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('periodes.update', $periode->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Année :</a>
                    <input type="text" name="annee" value="{{ $periode->annee }}" class="form-control" placeholder="Année">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Date début :</a>
                    <input type="date" name="date_debut_periode" class="form-control" value="{{$periode->date_debut}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Date Fin :</a>
                    <input type="date" name="date_fin_periode" class="form-control" value="{{$periode->date_fin}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Periode active :</a>
                    <div class="form-check">
                        @if($periode->active == "1")
                            <input class="form-check-input" type="radio" value="1" name="active" checked>
                        @else
                            <input class="form-check-input" type="radio" value="1" name="active">
                        @endif
                        <label class="form-check-label">Oui</label>
                    </div>
                    <div class="form-check">
                        @if ($periode->active == "0")
                            <input class="form-check-input" type="radio" value="0" name="active" checked>
                        @else
                            <input class="form-check-input" type="radio" value="0" name="active">
                        @endif
                        <label class="form-check-label">Non</label>
                    </div>    
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <a class="font-bold">Promotions :</a>
                @foreach($promotions as $p )
                <div class="form-check">
                    @if($periode->promotions->contains($p->id))
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]" checked="true">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @else
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @endif
                </div>
                @endforeach
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-outline-primary">Envoyer</button>
            </div>
        </div>
    </form>
@endsection