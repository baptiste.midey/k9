@extends('backend.template')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modifier un responsable de dispositif</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('rd.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups!</strong> Il y a des problèmes avec votre entrée.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('rd.update', $rd) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Nom:</a>
                    <input type="text" name="nom" id="nom" class="form-control" placeholder="Name" value="{{$rd->user->nom}}">
                </div>
                <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">prenom:</a>
                    <input type="text" name="prenom" id="prenom" class="form-control" placeholder="prenom" value="{{$rd->user->prenom}}">
                </div>
                <div class="form-group">
                    <a class="font-bold">adresse</a>
                    <input type="text" name="email" id="email" class="form-control" placeholder="adresse" value="{{$rd->user->email}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Promotion(s) associée(s) :</a>
                    @foreach($promotions as $p)
                    <div class="form-check">
                    @if($rd->promotions->contains($p->id))
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]" checked="true">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @else
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @endif
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Valider</button>
            </div>
        </div>
    </form>
@endsection