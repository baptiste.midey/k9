@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Responsables de dispositif</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('rd.create') }}">Ajouter un nouveau responsable de dispositif</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif
    
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Prénom</th>
                <th>Nom</th>
                <th>Email</th>
                <th>Promotion(s)</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
                @foreach ($rd as $rd)
                <tr>
                    <td>{{ $rd->id }}
                    <td>{{ $rd->user->nom }}</td>
                    <td>{{ $rd->user->prenom }}</td>
                    <td>{{ $rd->user->email }}</td>
                    <td>
                        @foreach ($rd->promotions as $p)
                            {{$p->libelle}},
                        @endforeach
                    </td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('rd.edit', $rd) }}"></a>
                        <a class="btn btn-show col" href="{{ route('rd.show', $rd) }}"></a>
                        <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$rd->id}}"></button>
                        <form action="{{ route('rd.destroy',$rd) }}" method="POST" class="col">
                            @csrf
                            @method('DELETE')
                            <div class="modal fade" id="{{ "modal-".$rd->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$rd->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="{{ "modalLabel-".$rd->id}}">
                                            Supression Responsable de dispositif</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        Voulez-vous vraiment supprimer ce responsable de dispositif ?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                        <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>
    </div>
@endsection