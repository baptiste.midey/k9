@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Ajouter un responsable de dispositif</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('rd.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups!</strong> Il y a des problèmes avec votre entrée.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('rd.store') }}" method="POST">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="nom" class="form-control" placeholder="nom" value="{{old('nom')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <strong>prenom:</strong>
                    <input class="form-control" name="prenom" placeholder="prenom" value="{{old('prenom')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <strong>Adresse</strong>
                    <input type="text" name="email" id="email" class="form-control" placeholder="adresse" value="{{old('email')}}">
                </div>
            </div>
            <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Valider</button>
            </div>
        </div>
    </div>
</form>
@endsection