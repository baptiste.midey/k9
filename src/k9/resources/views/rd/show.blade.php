@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Responsable de dispositif</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('rd.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom du responsable de dispositif :</a>
                {{ $rd->user->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Prénom du responsable de dispositif :</a>
                {{ $rd->user->prenom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Responsable de la promotion :</a>
                @foreach($rd->promotions as $p)
                {{ $p->libelle }},
                @endforeach
            </div>
        </div>
    </div>
@endsection     