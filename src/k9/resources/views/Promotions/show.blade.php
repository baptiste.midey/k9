@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Promotion</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('promotions.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Libelle :</a>
                {{ $promotion->libelle }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Date de début de la promotion :</a>
                {{ $promotion->date_debut }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Date de fin de la promotion :</a>
                {{ $promotion->date_fin }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Responsable de dispositif :</a>
                {{ $promotion->rd->user->prenom.' '.$promotion->rd->user->nom }}
            </div>
        </div>
    </div>
@endsection     