@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Promotions</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('promotions.create') }}"> Créer une Promotion</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif

    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Libelle</th>
                <th>Responsable de dispositif</th>
                <th>Date de début</th>
                <th>Date de fin</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
            @foreach ($promotions as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->libelle }}</td>
                    <td>{{ $p->rd->user->nom . ' ' . $p->rd->user->prenom }}</td>
                    <td>{{\Carbon\Carbon::parse($p->date_debut)->format('d/m/Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($p->date_fin)->format('d/m/Y')}}</td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('promotions.edit', $p) }}"></a>
                        <a class="btn btn-show col" href="{{ route('promotions.show', $p) }}"></a>
                    
                            <!-- Button trigger modal -->
                        <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$p->id}}"></button>
                        
                        <form action="{{ route('promotions.destroy',$p) }}" method="POST" >
                            @csrf
                            @method('DELETE')
                            <!-- Modal Suppression-->
                            <div class="modal fade" id="{{ "modal-".$p->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$p->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="{{ "modalLabel-".$p->id}}">
                                            Supression Promotion</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        Voulez-vous vraiment supprimer cette promotion?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                        <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

