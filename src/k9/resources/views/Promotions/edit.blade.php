@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>{{ !empty($promotion) ? 'Modifier' : 'Créer' }} une promotion</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('promotions.index') }}">Retour</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            Une erreur est survenue..<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @isset($promotion)
        <form action="{{ route('promotions.update', $promotion) }}" method="POST">
            @method('PUT')
        @else
            <form action="{{ route('promotions.store') }}" method="POST">
            @endisset
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Libelle :</a>
                            <input type="text" name="libelle" class="form-control" placeholder="SIO"
                                value="{{ !empty($promotion) ? $promotion->libelle : old('libelle') }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                          <a class="font-bold">Date de début de la promotion :</a>
                          <input type="date" name="date_debut" class="form-control" value="{{ !empty($promotion) ? $promotion->date_debut : (empty(old('date_debut')) ? Carbon\Carbon::now()->format('Y-m-d') : old('date_debut')) }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                          <a class="font-bold">Date de fin de la promotion :</a>
                          <input type="date" name="date_fin" class="form-control" value="{{ !empty($promotion) ? $promotion->date_fin : (empty(old('date_fin')) ? Carbon\Carbon::now()->format('Y-m-d') : old('date_fin')) }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Responsable de dispositif :</a>
                            <select class="form-select" name="rd_id" id="rd_id">
                                @foreach ($rds as $rd)
                                    <option value="{{ $rd->id }}"
                                        {{ !empty($promotion) ? ($promotion->rd->id == $rd->id ? 'selected' : '') : '' }}>
                                        {{ $rd->user->nom .' '.$rd->user->prenom }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </div>
            </div>
        </form>
    @endsection
