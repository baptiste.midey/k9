@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Aptitudes</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('aptitudes.create') }}"> Créer une aptitude</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Nom</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
                @foreach ($aptitude as $a)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $a->libelle }}</td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('aptitudes.edit',$a->id) }}"></a>   
                        <a class="btn btn-show col" href="{{ route('aptitudes.show',$a->id) }}"></a>    
                        <form action="{{ route('aptitudes.destroy',$a->id) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <button type="submit" class="btn btn-delete"></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>  
    </div>    
@endsection