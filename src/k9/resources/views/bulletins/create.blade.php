@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter un nouveau bulletin</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('bulletins.index') }}"> Retour </a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="bulletin" action="POST">
    @csrf
    <div class="col-12">
        <div class="form-group">
            <a class="font-bold">Promotions :</a>
            <select class="form-select" name="promotion_id" id="promotion_id">
                <option value="0"></option>
                @foreach($promotions as $promotion)
                    <option value="{{$promotion->id}}">{{ $promotion->libelle}} </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <a class="font-bold">Période : </a>
            <select name="periode" id="periode_id" class="form-select" disabled>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <a class="font-bold">Matieres : </a>
            <select name="matiere" id="matiere_id" class="form-select" disabled>
            </select>
        </div>
    </div>
    <div class="col-12 text-center">
        <button id="addMatiere" class="btn btn-success">Ajouter une matière</button>
    </div>
    <div class="padd20" id="table">
        <table class="table table-bordered" id="matieres">
            <thead>
                <th>Matière(s)</th>
            </thead>
            <tbody>
            </tbody>
        </table>      
    </div>
    <div class="col-12 text-center">
        <button type="submit" class="btn btn-primary">Créer le bulletin</button>
    </div>
</form>
@endsection
@section("script")
$(document).ready(function(event) {
    $('#promotion_id').change(function () { 
        var id = $('#promotion_id option:selected').val();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
        });
        $.ajax({
            type: 'POST',
            url: "{{route("promotionPeriode")}}",
            dataType: "json",
            data:{
                'id':id,
            },
            success: function (data) {
                if (data) {
                    $("#periode_id").attr('disabled', false);
                    $('#periode_id').empty();
                    $.each(data, function(key, periode) {
                        $('select[name="periode"]').append('<option value="'+ periode.id +'">Du '+ periode.date_debut+' Au '+periode.date_fin+'</option>');
                    });
                    $.ajaxSetup({
                        headers:{
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{route("promotionMatiere")}}",
                        dataType: "json",
                        data:{
                            'id':id,
                        },
                        success: function (data) {
                            if (data) {
                                $("#matiere_id").attr('disabled', false);
                                $('#matiere_id').empty();
                                $.each(data, function(key, matiere) {
                                    $('select[name="matiere"]').append('<option value="'+ matiere.id +'">'+ matiere.libelle +'</option>');
                                });
                            }else{
                                $('#matiere_id').empty();
                            }  
                        }
                    });
                }else{
                    $('#periode_id').empty();
                }  
            }
        });
    });
    $("#addMatiere").on("click", function(event) {
        event.preventDefault(); 
        var id = $('#matiere_id option:selected').val();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
        });
        $.ajax({
            type: 'POST',
            url: "{{route("bulletinMatiere")}}",
            dataType: "json",
            data:{
                'id':id,
            },
            success: function (data) {
                if (data) {
                    $('#matieres tbody').append('<tr><td>'+data.libelle+'<input type="hidden" name="matieres[]" value="'+data.id+'" /></td></tr>')
                }else{
                    $('#matiere_id').empty();
                }  
            }
        });
    });
    $("#bulletin").submit(function(event) {
        event.preventDefault(); 
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
        });
        $.ajax({
            type: 'POST',
            url: "{{route("bulletins.store")}}",
            dataType: "json",
            data: $("#bulletin").serialize(),
            success: function(data)
            {
                window.location=data.url;
            }
        });
    });
});
@endsection