@extends('backend.professeur')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Bulletin</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('bulletins.create') }}">Créer un nouveau bulletin</a>
                <a class="btn btn-outline-success" href="{{ route('bulletinPromotion') }}">Bulletins promotions</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20" id="table">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Année</th>
                <th>Période</th>
                <th>Classe</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach ($bulletins as $b)
                <tr>
                    <td>{{$b->id}}</td>
                    <td>{{$b->periode->annee}}</td>
                    <td>Du {{ \Carbon\Carbon::parse($b->date_debut)->format('d/m/Y')}} au {{ \Carbon\Carbon::parse($b->date_fin)->format('d/m/Y')}}</td>
                    @foreach ($b->periode->promotions as $p)                       
                        <td>{{$p->libelle}}</td>
                    @endforeach
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('bulletins.edit',$b->id) }}"></a>   
                        <a class="btn btn-show col" href="{{ route('bulletins.show',$b->id) }}"></a>    
                        <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$b->id}}"></button>
                        <form action="{{ route('entreprises.destroy',$b->id) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <div class="modal fade" id="{{ "modal-".$b->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$b->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="{{ "modalLabel-".$b->id}}">Supression entreprise</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez-vous vraimment supprimer ce bulletin?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                            <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>      
    </div>
@endsection