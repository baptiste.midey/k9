@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter un nouveau bulletin pour une classe</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('bulletins.index') }}"> Retour </a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('createPromoBulletin') }}" method="POST">
    @csrf
    <div class="col-12">
        <div class="form-group">
            <a class="font-bold">Promotions :</a>
            <select class="form-select" name="promotion_id" id="promotion_id">
                <option value="0"></option>
                @foreach($promotions as $promotion)
                    <option value="{{$promotion->id}}">{{ $promotion->libelle}} </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <a class="font-bold">Période : </a>
            <select name="periode" id="periode_id" class="form-select" disabled>
            </select>
        </div>
    </div>
    <div class="col-12 text-center">
        <button type="submit" class="btn btn-primary">Créer</button>
    </div>
</form>
@endsection
@section("script")
$(document).ready(function() {

    $('#promotion_id').change(function () { 
        var id = $('#promotion_id option:selected').val();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
        });
        $.ajax({
            type: 'POST',
            url: "{{route("promotionPeriode")}}",
            dataType: "json",
            data:{
                'id':id,
            },
            success: function (data) {
                if (data) {
                    $("#periode_id").attr('disabled', false);
                    $('#periode_id').empty();
                    $.each(data, function(key, periode) {
                        $('select[name="periode"]').append('<option value="'+ periode.id +'">Du '+ periode.date_debut+' Au '+periode.date_fin+'</option>');
                    });
                }else{
                    $('#periode_id').empty();
                }  
            }
        });
    });
});
@endsection