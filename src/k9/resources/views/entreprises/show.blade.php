@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Afficher l'entreprise</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('entreprises.index') }}">Retour </a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom :</a>
                {{ $entreprise->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Adresse :</a>
                {{ $entreprise->adresse }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Raison sociale : </a>
                {{ $entreprise->raison_sociale }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Téléphone : </a>
                {{ $entreprise->telephone }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Mail : </a>
                {{ $entreprise->user->email }}
            </div>
        </div>
    </div>
@endsection     