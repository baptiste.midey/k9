@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Entreprise</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('entreprises.create') }}">Créer un nouvelle entreprise</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20" id="table">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Nom</th>
                <th>Adresse</th>
                <th>Raison Sociale</th>
                <th>Téléphone</th>
                <th>Mail</th>
                <th width="250px">Action</th>
            </thead>
            <tbody>
                @foreach ($entreprise as $e)
                <tr>
                    <td>{{ $e->id }}</td>
                    <td>{{ $e->nom }}</td>
                    <td>{{ $e->adresse }}</td>
                    <td>{{ $e->raison_sociale }}</td>
                    <td>{{ $e->telephone }}</td>
                    @if ($e->user->email != null)
                    <td>{{ $e->user->email }}</td>
                    @endif
                    <td class="actions">
                        {{-- <a class="btn btn-edit col" href="{{ route('entreprises.edit',$e->id) }}"></a>    --}}
                        <a class="btn btn-show col" href="{{ route('entreprises.show',$e->id) }}"></a>    
                        <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$e->id}}"></button>
                        <form action="{{ route('entreprises.destroy',$e->id) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <div class="modal fade" id="{{ "modal-".$e->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$e->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="{{ "modalLabel-".$e->id}}">Supression entreprise</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez-vous vraimment supprimer cette entreprise?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                            <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>      
    </div>
@endsection