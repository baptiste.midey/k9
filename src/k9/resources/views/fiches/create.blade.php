@extends('backend.template')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Créer une fiche</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('entretiens.index') }}">Retour</a>
        </div>
    </div>
</div>
@if ($errors->any())
        <div class="alert alert-danger">
            Une erreur est survenue..<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
@endif
<form action="{{route('fiches.store')}}" method="POST">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Sélectionner l'entretien :</a>
                    <select class="form-control" name="entretien_id" id="entretien_id">
                        @foreach($entretiens as $entretien)
                        <option value="{{$entretien->id}}">{{ $entretien->apprenti->user->nom . " " . $entretien->apprenti->user->prenom}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-3">
                <div class="form-group">
                    <a class="font-bold">Demandeur d'emploi :</a>
                    <select class="form-control"  name="demandeur_emploi" id="demandeur_emploi">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                </div>
            </div>
            <div class="col-9">
                <div class="form-group">
                    <a class="font-bold">Autre demandeur :</a>
                    <input class="form-control"type="text" id="autre_demandeur" name="autre_demandeur">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Adéquation entre formation et projet :</a>
                    <select class="form-control"  name="formation_projet" name="formation_projet" id="formation_projet">
                        <option value="0">Conforme</option>
                        <option value="1">Non conforme</option>
                        <option value="2">Autre formation proposé</option>
                    </select>
                </div>   
            </div>    
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Connaissance et acceptation du métier :</a>
                    <select class="form-control"  name="connaissance" id="connaissance">
                        <option value="0">++</option>
                        <option value="1">+</option>
                        <option value="2">+/-</option>
                        <option value="3">-</option>
                        <option value="4">--</option>
                    </select>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Motivation :</a>
                    <select class="form-control"  name="motivation" id="motivation">
                        <option value="0">++</option>
                        <option value="1">+</option>
                        <option value="2">+/-</option>
                        <option value="3">-</option>
                        <option value="4">--</option>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Niveau scolaire :</a>
                    <select class="form-control"  name="niveau_scolaire" id="niveau_scolaire">
                        <option value="0">< 10</option>
                        <option value="1">10-12</option>
                        <option value="2">12-15</option>
                        <option value="3"> >15</option>
                        <option value="4">Non concerné</option>
                    </select>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Experience professionnelle ou associative pouvant être mise en avant auprès d'un futur employeur :</a>
                    <select class="form-control"  name="experience_pro" id="experience_pro">
                        <option value="0">Oui significative</option>
                        <option value="1">Oui</option>
                        <option value="2">Non</option>
                        <option value="3">-</option>
                        <option value="4">--</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Précisions quant à l'expérience :</a>    
                    <textarea class="form-control" id="experience_pro_text" name="experience_pro_text" ></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Atouts :</a>    
                    <textarea class="form-control"id="atouts" name="atouts"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Difficultés envisageables et solutions possibles :</a>
                    <textarea class="form-control" id="difficultes_solutions" name="difficultes_solutions"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Entreprise trouvée :</a>
                    <select class="form-control"  name="entreprise_trouvee" id="entreprise_trouvee">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Si oui, poste conforme à la formation :</a>
                    <select class="form-control"  name="post_conforme" id="post_conforme">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Démarches effectutées :</a>
                    <select class="form-control"  name="demarche" id="demarche">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Type d'entreprise souhaité :</a>
                    <input class="form-control"type="text" id="type_entreprise" name="type_entreprise">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Sécteur(s) géographique(s) de recherche pour l'entreprise d'accueil :</a>
                    <textarea class="form-control" id="secteur_geo" name="secteur_geo"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Bilan :</a>
                    <textarea class="form-control" id="bilan" name="bilan"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Aménagements spécifiques de la scolarité et/ou de l'examen :</a>
                    <select class="form-control"  name="amenagements" id="amenagements">
                        <option value="0">PAI</option>
                        <option value="1">PPS</option>
                        <option value="2">PAP</option>
                        <option value="3">Autre</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Permis de conduire :</a>    
                    <select class="form-control"  name="permis" id="permis">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                        <option value="1">En cours</option>
                    </select> 
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Véhicule :</a>
                    <select class="form-control"  name="vehicule" id="vehicule">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Décision prise à l'égard du candidat :</a>    
                    <textarea class="form-control" id="decision" name="decision" ></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Résultat de l'entretien :</a>    
                    <select class="form-select" name="decision2">
                        <option value="2">Accepté</option>
                        <option value="1">En cours</option>
                        <option value="0">Refusé</option>
                      </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Enregistrer la fiche</button>
            </div>
        </div>    
    </div>
</form>
@endsection