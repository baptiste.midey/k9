@extends('backend.template')

@section('title')
Fiches
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>CRUD Fiches</h2>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@elseif($errors->any())
    <div class="alert alert-warning">
        {{$errors->first()}}
    </div>
@endif
<div class="padd20">
       <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Candidat concerné</th>
                <th>Date de l'entretien</th>
                <th>Décision de l'entretien</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
                @foreach ($fiches as $fiche)
                    <tr>
                        <td>{{$fiche->id}}</td>
                        @isset($fiche->entretien)
                        <td>{{$fiche->entretien->apprenti->user->prenom . " " . $fiche->entretien->apprenti->user->nom}}</td>
                        <td>{{\Carbon\Carbon::parse($fiche->entretien->date_entretien)->format('d/m/Y')}}</td>
                        <td>{{$fiche->entretien->decision ? "Accepté" : "Refusé" }}</td>
                        @else
                        <td>{{$fiche->entretien}}</td>
                        <td>null</td>
                        <td>null</td>
                        @endisset
                        <td class="actions">
                            <a class="btn btn-edit col" href="{{ route('fiches.edit',$fiche->id) }}"></a>
                            <a class="btn btn-pdf col" href="{{ route('getPostPdf',$fiche->id) }}"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
       </table>
</div>
@endsection
