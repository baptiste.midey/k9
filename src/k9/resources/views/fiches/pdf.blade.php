<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <h2 class="yellow-back">Compte rendu de l'entretien de recrutement pour une formation en alternance</h2>
    {{-- Block identification de l'élève --}}
    <div class="block_eleve">
        <h3>Identification du candidat(e) :</h3>
        <p class="p1">Nom : {{$fiche->entretien->apprenti->user->nom}}</p>
        <p class="p1">Prénom : {{$fiche->entretien->apprenti->user->prenom}}</p>
        <p class="p1">Adresse : {{$fiche->entretien->apprenti->adresse}}</p>
        <p class="p1">Telephone : {{$fiche->entretien->apprenti->telephone}}</p>
        <p class="p1">Mail : {{$fiche->entretien->apprenti->user->email}}</p>
        <p class="p1">Formation actuelle : {{$fiche->entretien->apprenti->formation_actuelle}}</p>
        <p class="p1">Diplôme le plus haut obtenu : {{$fiche->entretien->apprenti->diplome}}</p>
        <p class="p1">Spécialités suivies au lycée : {{$fiche->entretien->apprenti->specialite_lycee}}</p>
    </div>
    {{-- Block formation souhaitée --}}
    <div class="block_formation">
        <h3>Formation souhaitée : {{$fiche->entretien->apprenti->promotion->libelle}}</h3>
        <p class="p1">LV1 : {{$fiche->entretien->apprenti->langue_vivante_1}}</p>
        <p class="p1">LV2 : {{$fiche->entretien->apprenti->langue_vivante_2}}</p>
        @if ($fiche->entretien->apprenti->promotion->libelle == "BTS SIO")
            <p class="p1">Option (Pour SIO) :</p>
            <div class="option">
                <input type="checkbox" id="sisr" name="emploi">
                <label for="emploi">SISR</label>
                <input type="checkbox" id="slam" name="emploi">
                <label for="emploi">SLAM</label>
            </div>
        @else
            {{-- <p class="p1">Option (Pour SIO) :</p>
            <p class="p1">SISR/SLAM</p> --}}
        @endif
    </div>
    {{-- Block entretien --}}
    <div class="emploi">
        <h3>Le candidat est il demandeur d'emploi ?</h3>
        <p>
        @if ($fiche->demandeur_emploi == 1)
            <input type="checkbox" id="emploi" name="emploi" checked>
            <label for="emploi2">Oui</label> 
        @else
            <input type="checkbox" id="emploi2" name="emploi">
            <label for="emploi2">Oui</label> 
        @endif 
        @if ($fiche->demandeur_emploi == 0 )
            <input type="checkbox" id="emploi3" name="emploi" checked>
            <label for="emploi">Non</label>
        @else
            <input type="checkbox" id="emploi4" name="emploi">
            <label for="emploi">Non</label>            
        @endif
        </p>
    </div>
    <div class="emploi_autre">
        <h3>Autre :</h3> 
        <p>{{ $fiche->autre_demandeur}}</p>
    </div>
    <h3 class="candidature">Elements objectifs receuillis lors de l'entretient de motivation et nécessaires au traitement de la candidature : </h3>
    <div class="projet">
        <p>Adéquation entre la formation et le projet :&nbsp;
        @if ($fiche->formation_projet == 1)
            <input type="checkbox" id="emploi5" name="emploi"checked>
            <label for="emploi">Conforme</label>
        @else
            <input type="checkbox" id="emploi6" name="emploi">
            <label for="emploi">Conforme</label>
        @endif
        @if ($fiche->formation_projet == 0)
            <input type="checkbox" id="emploi7" name="emploi"checked>
            <label for="emploi">Non conforme</label>
        @else 
            <input type="checkbox" id="emploi8" name="emploi">
            <label for="emploi">Non conforme</label>
        @endif
        @if ($fiche->formation_projet == 2)
            <input type="checkbox" id="emploi9" name="emploi"checked>
            <label for="emploi">Autre orientation proposée</label>
        @else
            <input type="checkbox" id="emploi10" name="emploi">
            <label for="emploi">Autre orientation proposée</label>
        @endif
        </p>
    </div>
    <div class="metier">
        <p>Connaissance et acceptation du métier :&nbsp;
        @if ($fiche->connaissance == 0)
            <input type="checkbox" id="conn" name="conn"checked>
            <label for="conn">--</label>
        @else
            <input type="checkbox" id="conn1" name="conn">
            <label for="conn">--</label>
        @endif
        @if ($fiche->connaissance == 1)
            <input type="checkbox" id="conn2" name="conn"checked>
            <label for="conn">-</label>
        @else
            <input type="checkbox" id="conn3" name="conn">
            <label for="conn">-</label>
        @endif
        @if ($fiche->connaissance == 2)
            <input type="checkbox" id="conn4" name="conn"checked>
            <label for="conn">+/-</label>
        @else
            <input type="checkbox" id="conn5" name="conn">
            <label for="conn">+/-</label>
        @endif
        @if ($fiche->connaissance == 3)
            <input type="checkbox" id="conn6" name="conn"checked>
            <label for="conn">+</label>
        @else
            <input type="checkbox" id="conn7" name="conn">
            <label for="conn">+</label>
        @endif
        @if ($fiche->connaissance == 4)
            <input type="checkbox" id="conn8" name="conn"checked>
            <label for="conn">++</label>
        @else
            <input type="checkbox" id="conn9" name="conn">
            <label for="conn">++</label>
        @endif
        </p>
    <div class="motivation">
        <p>Motivation :&nbsp;
        @if ($fiche->motivation == 0)
            <input type="checkbox" id="motiv" name="motiv"checked>
            <label for="motiv">++</label>
        @else
            <input type="checkbox" id="motiv1" name="motiv">
            <label for="motiv">++</label>
        @endif
        @if ($fiche->motivation == 1)
            <input type="checkbox" id="motiv2" name="motiv"checked>
            <label for="motiv">+</label>
        @else
            <input type="checkbox" id="motiv3" name="motiv">
            <label for="motiv">+</label>
        @endif
        @if ($fiche->motivation == 2)
            <input type="checkbox" id="motiv4" name="motiv"checked>
            <label for="motiv">+/-</label>
        @else
            <input type="checkbox" id="motiv5" name="motiv">
            <label for="motiv">+/-</label>
        @endif
        @if ($fiche->motivation == 3)
            <input type="checkbox" id="motiv6" name="motiv"checked>
            <label for="motiv">-</label>
        @else
            <input type="checkbox" id="motiv7" name="motiv">
            <label for="motiv">-</label>
        @endif
        @if ($fiche->motivation == 4)
            <input type="checkbox" id="motiv8" name="motiv"checked>
            <label for="motiv">--</label>
        @else
            <input type="checkbox" id="motiv9" name="motiv">
            <label for="motiv">--</label>
        @endif
        </p>
    </div>
    <div class="scolaire"> 
        <p>Niveau scolaire :&nbsp; 
        @if ($fiche->niveau_scolaire == 0)
            <input type="checkbox" id="scol" name="scol"checked>
            <label for="scol">&lsaquo;10</label>
        @else
            <input type="checkbox" id="scol1" name="scol">
            <label for="scol">&lsaquo;10</label>
        @endif
        @if ($fiche->niveau_scolaire == 1)
            <input type="checkbox" id="scol2" name="scol"checked>
            <label for="scol">10-12</label>
        @else
            <input type="checkbox" id="scol3" name="scol">
            <label for="scol">10-12</label>
        @endif
        @if ($fiche->niveau_scolaire == 2)
            <input type="checkbox" id="scol4" name="scol"checked>
            <label for="scol">12-15</label>
        @else
            <input type="checkbox" id="scol5" name="scol">
            <label for="scol">12-15</label>
        @endif
        @if ($fiche->niveau_scolaire == 3)
            <input type="checkbox" id="scol6" name="scol"checked>
            <label for="scol">&rsaquo;15</label>
        @else
            <input type="checkbox" id="scol7" name="scol">
            <label for="scol">&rsaquo;15</label>
        @endif
        </p>
    </div>
    <div class="experience">
        <p>Experience professionnelle ou associative pouvant être mise en avant auprès d'un futur employeur :</p>
        <p>
        @if ($fiche->experience_pro == 0)
            <input type="checkbox" id="exp" name="exp"checked>
            <label for="exp">Oui significative</label>
        @else
            <input type="checkbox" id="exp1" name="exp">
            <label for="exp">Oui significative</label>
        @endif
        @if ($fiche->experience_pro == 1)
            <input type="checkbox" id="exp2" name="exp"checked>
            <label for="exp">Oui</label>
        @else
            <input type="checkbox" id="exp3" name="exp">
            <label for="exp">Oui</label>
        @endif
        @if ($fiche->experience_pro == 2)
            <input type="checkbox" id="exp4" name="exp"checked>
            <label for="exp">Non</label>
        @else
            <input type="checkbox" id="exp5" name="exp">
            <label for="exp">Non</label>
        @endif
        @if ($fiche->experience_pro == 3)
            <input type="checkbox" id="exp6" name="exp"checked>
            <label for="exp">-</label>
        @else
            <input type="checkbox" id="exp7" name="exp">
            <label for="exp">-</label>
        @endif
        @if ($fiche->experience_pro == 4)
            <input type="checkbox" id="exp8" name="exp"checked>
            <label for="exp">--</label>
        @else
            <input type="checkbox" id="exp9" name="exp">
            <label for="exp">--</label>
        @endif
        </p>
    </div>
    <div class="experience_pro">
        Précision quant à l'experience professionnelle : <p class="p1">{{$fiche->experience_pro_text}}</p>
    </div>
    <div class="atouts">
        Atouts : <p>{{$fiche->atouts}}</p>
    </div>
    Difficultés envisageables et solutions possibles :
    {{$fiche->difficultes_solutions}}
    <div class="page2">
        <h2 class="yellow-back">Compte rendu de l'entretien de recrutement pour une formation en alternance</h2>
    </div>
    <div class="recherche">
        <h3>Recherche pour l'entreprise d'accueil : </h3>
    </div>
    <div class="entreprise">
        <p>Entreprise trouvée :&nbsp; 
        @if ($fiche->formation_projet == 0)
            <input type="checkbox" id="form" name="form" checked>
            <label for="form">Oui</label>
        @else
            <input type="checkbox" id="form1" name="form">
            <label for="form">Oui</label>
        @endif
        @if ($fiche->formation_projet == 1)
            <input type="checkbox" id="form2" name="form"checked>
            <label for="form">Non</label>
        @else
            <input type="checkbox" id="form3" name="form">
            <label for="form">Non</label>
        @endif
        </p>
    </div>
    <div class="poste">
        <p>Si oui, poste conforme à la formation :&nbsp;
        @if ($fiche->poste_conforme == 0)
            <input type="checkbox" id="poste" name="poste"checked>
            <label for="poste">Oui</label>
        @else
            <input type="checkbox" id="poste1" name="poste">
            <label for="poste">Oui</label>
        @endif
        @if ($fiche->formation_projet == 1)
            <input type="checkbox" id="poste2" name="poste"checked>
            <label for="poste">Non</label>
        @else
            <input type="checkbox" id="poste3" name="poste">
            <label for="poste">Non</label>
        @endif
        </p>
    </div>
    <div class="demarche">
        <p>Démarche effectué :&nbsp;
        @if ($fiche->demarche == 0)
            <input type="checkbox" id="demarche" name="demarche"checked>
            <label for="form">Oui</label>
        @else
            <input type="checkbox" id="demarche1" name="demarche">
            <label for="form">Oui</label>
        @endif
        @if ($fiche->demarche == 1)
            <input type="checkbox" id="demarche2" name="demarche"checked>
            <label for="form">Non</label>
        @else
            <input type="checkbox" id="demarche3" name="demarche">
            <label for="form">Non</label>
        @endif
        </p>
    </div>
    <div class="type_entreprise">Type d'entreprise souhaité : {{$fiche->type_entreprise}}</div>
    <div class="geographique">
        Secteur(s) géographique(s) de recherche pour l'entreprise d'accueil : <p>{{$fiche->secteur_geo}}</p>
    </div>
    <div class="bilan">
        <h3>Bilan : </h3>
        <p>{{ $fiche->bilan}}</p>
    </div>
    <div class="amenagement">
        <h3>Aménagement spécifique de la scolarité et/ou de l'examen : </h3>
        <p>
            @if ($fiche->amenagements == 0)
                <input type="checkbox" id="amg" name="amg"checked>
                <label for="exp">PAI</label>
            @else
                <input type="checkbox" id="amg1" name="amg">
                <label for="exp">PAI</label>
            @endif
            @if ($fiche->amenagements == 1)
                <input type="checkbox" id="amg2" name="amg"checked>
                <label for="exp">PPS</label>
            @else
                <input type="checkbox" id="amg3" name="amg">
                <label for="exp">PPS</label>
            @endif
            @if ($fiche->amenagements == 2)
                <input type="checkbox" id="amg4" name="amg"checked>
                <label for="exp">PAP</label>
            @else
                <input type="checkbox" id="amg5" name="amg">
                <label for="exp">PAP</label>
            @endif
            @if ($fiche->amenagements == 3)
                <input type="checkbox" id="amg6" name="amg"checked>
                <label for="exp">Autre(s),précisez</label>
            @else
                <input type="checkbox" id="amg7" name="amg">
                <label for="exp">Autre(s),précisez</label>
            @endif
            </p>
    </div>  
    <div class="permis">
        <h3>Permis de conduire</h3>
        <p>
            @if ($fiche->permis == 1)
                <input type="checkbox" id="perm2" name="perm"checked>
                <label for="exp">Oui</label>
            @else
                <input type="checkbox" id="perm3" name="perm">
                <label for="exp">Oui</label>
            @endif
            @if ($fiche->permis == 0)
                <input type="checkbox" id="perm" name="perm"checked>
                <label for="exp">Non</label>
            @else
                <input type="checkbox" id="perm1" name="perm">
                <label for="exp">Non</label>
            @endif
            @if ($fiche->permis == 2)
                <input type="checkbox" id="perm4" name="perm"checked>
                <label for="exp">En cours</label>
            @else
                <input type="checkbox" id="perm5" name="perm">
                <label for="exp">En cours</label>
            @endif
            </p>
    </div>
    <div class="vehicule">
        <h3>Véhicule</h3>
        @if ($fiche->vehicule == 1)
            <input type="checkbox" id="veh" name="veh"checked>
            <label for="exp">Oui</label>
        @else
            <input type="checkbox" id="veh1" name="veh">
            <label for="exp">Oui</label>
        @endif
        @if ($fiche->vehicule == 0)
            <input type="checkbox" id="veh2" name="veh"checked>
            <label for="exp">Non</label>
        @else
            <input type="checkbox" id="veh3" name="veh">
            <label for="exp">Non</label>
        @endif
    </div>
    <div class="decision">
        <h3>Décision prise à l'égard du candidat :</h3>
        @if ($fiche->entretien->decision == 2)
            <p>Accepté</p>
        @elseif ($fiche->entretien->decision == 1)
            <p>Refusé</p>
        @endif
    </div>
</body>
<style>
body{font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; font-size: 12px;}
h3{
    color :rgb(66, 122, 172);
    font-size: 14px
}
.p1 {
    margin-bottom: 0.5px;
}
.block_formation {
    margin-left: 55%;
    transform: translateY(-245px);
}
.block_formation input {
    transform: translateY(5px);
}
.option {
    margin-top: 2%;
}
.emploi{
    transform: translateY(-90px);
    margin-top: -4%;
    margin-bottom: 4%;
}
.emploi p {
    margin-left : 55%;
    margin-top: -10%;
}
.emploi input {
    transform: translateY(5px);
}
.emploi_autre {
    transform: translateY(-120px);
}
.candidature {
    transform: translateY(-100px);
}
.projet {
    transform: translateY(-100px);
}
.projet input {
    transform: translateY(5px);
}
.metier {
    transform: translateY(-110px);
}
.metier input {
    transform: translateY(5px);
}
.motivation input {
    transform: translateY(5px);
}
.scolaire input {
    transform: translateY(5px);
}
/* .experience_pro {
   transform : translateY(5px)
} */
.page2 {
    transform : translateY(80px);
}
.recherche {
    transform: translateY(70px);
}
.entreprise {
    transform: translateY(70px);
}
.poste {
    transform: translateY(50px);
}
.demarche {
    transform: translateY(30px);
}
.type_entreprise {
    transform: translateY(30px);
}
.geographique {
    transform: translateY(30px);
}
.bilan {
    transform: translateY(40px);
}
.amenagement {
    transform: translateY(100px);
}
.amenagement input {
    transform: translateY(5px);
}
.permis {
    transform: translateY(80px);
}
.vehicule {
    transform: translateY(80px);
}
.decision h3{
    transform: translateY(80px);
    background-color : rgb(255, 255, 92);
}
.decision p {
    transform: translateY(80px);
}
.yellow-back{background-color : rgb(255, 255, 92);}
</style>
</html>
