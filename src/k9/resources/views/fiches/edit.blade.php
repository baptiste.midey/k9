@extends('backend.template')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Modification de la fiche</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('fiches.index') }}">Retour </a>
        </div>
    </div>
</div>

@if ($errors->any())
        <div class="alert alert-danger">
            Une erreur est survenue..<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
@endif
<form action="{{route('fiches.update', $fiche->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="container">
        <div class="row"> 
            <div class="col-3">
                <div class="form-group">
                    <a class="font-bold">Demandeur d'emploi :</a>
                    <select class="form-control"  name="demandeur_emploi" id="demandeur_emploi">
                        <option value="0" {{ $fiche->demandeur_emploi == 0 ? 'selected' : '' }}>Non</option>
                        <option value="1" {{ $fiche->demandeur_emploi == 1 ? 'selected' : '' }}>Oui</option>
                    </select>
                </div>
            </div>
            <div class="col-9">
                <div class="form-group">
                    <a class="font-bold"">Autre demandeur :</label>
                    <input class="form-control"type="text" id="autre_demandeur" name="autre_demandeur" value="{{$fiche->autre_demandeur}}">
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Adéquation entre formation et projet :</a>
                    <select class="form-control"  name="formation_projet" name="formation_projet" id="formation_projet">
                        <option value="0" {{ $fiche->formation_projet == 0 ? 'selected' : '' }}>Conforme</option>
                        <option value="1" {{ $fiche->formation_projet == 1 ? 'selected' : '' }}>Non conforme</option>
                        <option value="2" {{ $fiche->formation_projet == 2 ? 'selected' : '' }}>Autre formation proposé</option>
                    </select>
                </div>   
            </div>
            
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Connaissance et acceptation du métier :</a>
                    <select class="form-control"  name="connaissance" id="connaissance">
                        <option value="0" {{ $fiche->connaissance == 0 ? 'selected' : '' }}>++</option>
                        <option value="1" {{ $fiche->connaissance == 1 ? 'selected' : '' }}>+</option>
                        <option value="2" {{ $fiche->connaissance == 2 ? 'selected' : '' }}>+/-</option>
                        <option value="3" {{ $fiche->connaissance == 3 ? 'selected' : '' }}>-</option>
                        <option value="4" {{ $fiche->connaissance == 4 ? 'selected' : '' }}>--</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Motivation :</a>
                    <select class="form-control"  name="motivation" id="motivation">
                        <option value="0" {{ $fiche->motivation == 0 ? 'selected' : '' }}>++</option>
                        <option value="1" {{ $fiche->motivation == 1 ? 'selected' : '' }}>+</option>
                        <option value="2" {{ $fiche->motivation == 2 ? 'selected' : '' }}>+/-</option>
                        <option value="3" {{ $fiche->motivation == 3 ? 'selected' : '' }}>-</option>
                        <option value="4" {{ $fiche->motivation == 4 ? 'selected' : '' }}>--</option>
                    </select>
                </div>
            </div>
            
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Niveau scolaire :</label>
                    <select class="form-control"  name="niveau_scolaire" id="niveau_scolaire">
                        <option value="0" {{ $fiche->niveau_scolaire == 0 ? 'selected' : '' }}><10</option>
                        <option value="1" {{ $fiche->niveau_scolaire == 1 ? 'selected' : '' }}>10-12</option>
                        <option value="2" {{ $fiche->niveau_scolaire == 2 ? 'selected' : '' }}>12-15</option>
                        <option value="3" {{ $fiche->niveau_scolaire == 3 ? 'selected' : '' }}>>15</option>
                        <option value="4" {{ $fiche->niveau_scolaire == 4 ? 'selected' : '' }}>Non concerné</option>
                    </select>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Experience professionnelle ou associative pouvant être mise en avant auprès d'un futur employeur :</a>
                    <select class="form-control"  name="experience_pro" id="experience_pro">
                        <option value="0" {{ $fiche->experience_pro == 0 ? 'selected' : '' }}>Oui significative</option>
                        <option value="1" {{ $fiche->experience_pro == 1 ? 'selected' : '' }}>Oui</option>
                        <option value="2" {{ $fiche->experience_pro == 2 ? 'selected' : '' }}>Non</option>
                        <option value="3" {{ $fiche->experience_pro == 3 ? 'selected' : '' }}>-</option>
                        <option value="4" {{ $fiche->experience_pro == 4 ? 'selected' : '' }}>--</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Précisions quant à l'expérience :</a>  
                    <textarea class="form-control" id="experience_pro_text" name="experience_pro_text">{{$fiche->experience_pro_text}}</textarea>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Atouts :</a>  
                    <textarea class="form-control"id="atouts" name="atouts" placeholder="atouts">{{$fiche->atouts}}</textarea>    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Difficultés envisageables et solutions possibles :</a>
                    <textarea class="form-control" id="difficultes_solutions" name="difficultes_solutions">{{$fiche->difficultes_solutions}}</textarea>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Entreprise trouvée :</a>
                    <select class="form-control"  name="entreprise_trouvee" id="entreprise_trouvee">
                        <option value="0" {{ $fiche->entreprise_trouvee == 0 ? 'selected' : '' }}>Non</option>
                        <option value="1" {{ $fiche->entreprise_trouvee == 1 ? 'selected' : '' }}>Oui</option>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Si oui, poste conforme à la formation :</a>
                    <select class="form-control"  name="post_conforme" id="post_conforme">
                        <option value="0" {{ $fiche->post_conforme == 0 ? 'selected' : '' }}>Non</option>
                        <option value="1" {{ $fiche->post_conforme == 1 ? 'selected' : '' }}>Oui</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Démarches effectutées :</a>
                    <select class="form-control"  name="demarche" id="demarche">
                        <option value="0" {{ $fiche->demarche == 0 ? 'selected' : '' }}>Non</option>
                        <option value="1" {{ $fiche->demarche == 1 ? 'selected' : '' }}>Oui</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Type d'entreprise souhaité :</a>
                    <input class="form-control"type="text" id="type_entreprise" name="type_entreprise" value="{{$fiche->type_entreprise}}">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Sécteur(s) géographique(s) de recherche pour l'entreprise d'accueil :</a>
                    <textarea class="form-control" id="secteur_geo" name="secteur_geo">{{$fiche->secteur_geo}}</textarea>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Bilan :</a>
                    <textarea class="form-control" id="bilan" name="bilan">{{$fiche->bilan}}</textarea>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Aménagements spécifiques de la scolarité et/ou de l'examen :</a>
                    <select class="form-control"  name="amenagements" id="amenagements">
                        <option value="0" {{ $fiche->amenagements == 0 ? 'selected' : '' }}>PAI</option>
                        <option value="1" {{ $fiche->amenagements == 1 ? 'selected' : '' }}>PPS</option>
                        <option value="2" {{ $fiche->amenagements == 3 ? 'selected' : '' }}>PAP</option>
                        <option value="3" {{ $fiche->amenagements == 3 ? 'selected' : '' }}>Autre</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Permis de conduire :</a>   
                    <select class="form-control"  name="permis" id="permis">
                        <option value="0" {{ $fiche->permis == 0 ? 'selected' : '' }}>Non</option>
                        <option value="1" {{ $fiche->permis == 1 ? 'selected' : '' }}>Oui</option>
                        <option value="2" {{ $fiche->permis == 3 ? 'selected' : '' }}>En cours</option>
                    </select>  
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <a class="font-bold">Véhicule :</a>
                    <select class="form-control"  name="vehicule" id="vehicule">
                        <option value="0" {{ $fiche->vehicule == 0 ? 'selected' : '' }}>Non</option>
                        <option value="1" {{ $fiche->vehicule == 1 ? 'selected' : '' }}>Oui</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Décision prise à l'égard du candidat :</a>    
                    <textarea class="form-control" id="decision" name="decision" >{{$fiche->decision}}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Enregistrer les modifications</button>
            </div>
        </div>
    </div>
</form>
@endsection