@extends('template')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-start">
            <h2>Mon profil</h2>
        </div>
    </div>
</div>
<div class="row">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
<form action="{{ route('profilUpdate') }}" method="POST">
    @csrf
    @if ($user->profil_type == "App\Models\Apprenti")
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="nom" class="form-label">Nom :</label>
                    <input type="text" name="nom" value="{{ $user->nom }}" class="form-control" placeholder="Saisir un nom">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="prenom" class="form-label">Prenom :</label>
                    <input type="text" name="prenom" value="{{ $user->prenom }}" class="form-control" placeholder="Saisir un Prenom">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="email" class="form-label">Email :</label>
                    <input type="text" name="email" id="email" value="{{ $user->email }}" class="form-control" placeholder="Saisir un email">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="telephone" class="form-label">Téléphone :</label>
                    <input type="text" name="telephone" id="telephone" value="{{ $user->profil->telephone }}" class="form-control" placeholder="Saisir un téléphone">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="adresse" class="form-label">Adresse :</label>
                    <input type="text" name="adresse" id="adresse" value="{{ $user->profil->adresse }}" class="form-control" placeholder="Saisir une adresse">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="ville" class="form-label">Ville :</label>
                    <input type="text" name="ville" id="ville" value="{{ $user->profil->ville }}" class="form-control" placeholder="Saisir une ville">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="code_postal" class="form-label">Code Postal :</label>
                    <input type="text" name="code_postal" id="code_postal" value="{{ $user->profil->code_postal }}" class="form-control" placeholder="Saisir un code postal">
                </div>
            </div>
        </div>
        @if($entretien != null)
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="entretien" class="form-label">Résultat de votre entretien :</label>
                    @foreach ($entretien as $e)
                        @if($e->decision == 2)
                            <input type="text" name="entretien" id="entretien" value="Accepté(e) pour la formation {{$e->promotion->libelle}}" class="form-control" readonly="readonly">
                        @elseif($e->decision == 1)
                            <input type="text" name="entretien" id="entretien" value="Refusé(e) pour la formation {{$e->promotion->libelle}}" class="form-control" readonly="readonly">
                        @endif
                    @endforeach
                </div>
            </div>    
        </div>
        @endif
    @endif
    @if ($user->profil_type == "App\Models\Entreprise")
        <div class="col-12">
            <div class="form-group">
                <label for="nom_entr" class="form-label">Nom de l'entreprise :</label>
                <input type="text" name="nom_entr" value="{{ $user->nom }}" class="form-control" placeholder="Saisir un nom">
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="tel_entr" class="form-label">Téléphone de l'entreprise :</label>
                    <input type="text" name="tel_entr" value="{{ $user->profil->telephone }}" class="form-control" placeholder="Saisir un téléphone">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="email" class="form-label">Email de l'entreprise :</label>
                    <input type="text" name="email" id="email" value="{{ $user->email }}" class="form-control" placeholder="Saisir un email">
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label for="raison_entr" class="form-label">Raison sociale de l'entreprise :</label>
                <input type="text" name="raison_entr" value="{{ $user->profil->raison_sociale }}" class="form-control" placeholder="Saisir une raison sociale">
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label for="adresse_entr" class="form-label">Adresse  de l'entreprise :</label>
                <input type="text" name="adresse_entr" value="{{ $user->profil->adresse }}" class="form-control" placeholder="Saisir une adresse">
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="mdp" class="form-label">Nouveau Mot de passe :</label>
                <input type="password" name="password" value="" class="form-control" placeholder="Saisir un mot de passe">
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label for="mdpconf" class="form-label">Confirmer le nouveau mot de passe :</label>
                <input type="password" name="password_confirmation" value="" class="form-control" placeholder="Confirmer le mot de passe">
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 pt-4">
        <button type="submit" class="btn btn-outline-primary">Enregistrer</button>
    </div>
</form>
<form action="{{ route('users.destroy',$user->id) }}" method="POST">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-outline-danger">Supprimer mon profil</button>
</form>
@endsection