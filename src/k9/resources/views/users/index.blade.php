@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Users</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('users.create') }}"> Créer un utilisateur</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <div class="padd20">
        <table class="table table-bordered">
            <thead>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Email</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
                @foreach ($user as $u)
                <tr>   
                    <td>{{ $u->nom }}</td>
                    <td>{{ $u->prenom }}</td>
                    <td>{{ $u->email }}</td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('users.edit',$u->id) }}"></a>
                        <a class="btn btn-show col" href="{{ route('users.show',$u->id) }}"></a>
                        <form action="{{ route('users.destroy',$u->id) }}" method="POST" class="col">       
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-delete"></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection