@extends('backend.template')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modifier un utilisateur</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('users.index') }}"> Retour</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('users.update',$user->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>nom</strong>
                <input type="text" name="nom" value="{{ $user->nom }}"class="form-control" placeholder="Saisir un nom">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>prenom:</strong>
                <input type="text" name="prenom" value="{{ $user->prenom }}" class="form-control" placeholder="Saisir un Prenom">
            </div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>email:</strong>
                <input type="text" name="email" value="{{ $user->email }}" class="form-control" placeholder="Saisir un email">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center pt-4">
              <button type="submit" class="btn btn-outline-primary">Soumettre</button>
            </div>
        </div>
   
    </form>
@endsection