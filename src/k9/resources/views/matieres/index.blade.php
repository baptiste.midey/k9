@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Matieres</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success"  href="{{ route('matieres.create') }}">Créer une matière</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Nom</th>
                <th>Coefficient</th>
                <th>Code</th>
                <th>Professeur</th>
                <th>Matiere</th>
                <th>Promotions</th>
                <th width="220px">Action</th>
            </thead>
            <tbody>
                @foreach ($matieres as $m)
                <tr>
                    <td>{{ $m->id }}</td>
                    <td>{{ $m->libelle }}</td>
                    <td>{{ $m->coefficient }}</td>
                    <td>{{ $m->code}}</td>
                    <td>{{ $m->professeur->user->nom }} {{ $m->professeur->user->prenom }}</td>
                    @if(isset($m->parent))
                    <td>{{ $m->parent->libelle}}</td>
                    @else
                        <td></td>
                    @endif
                    <td>
                        @foreach($m->promotions as $promotion)
                            {{ $promotion->libelle }},
                        @endforeach
                    </td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('matieres.edit',$m->id) }}"></a>   
                        <a class="btn btn-show col" href="{{ route('matieres.show',$m->id) }}"></a>    
                        <form action="{{ route('matieres.destroy',$m->id) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <button type="submit" class="btn btn-delete"></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>  
    </div>    
@endsection