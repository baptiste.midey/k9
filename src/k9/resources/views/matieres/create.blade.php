@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter une nouvelle Matière</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('matieres.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('matieres.store') }}" method="POST" enctype="multipart/form-data">
   @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Libelle :</a>
                    <input type="text" name="libelle" class="form-control" placeholder="Entrez un nom">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Coefficient :</a>
                    <input type="text" name="coefficient" class="form-control" placeholder="Entrez un coefficient">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Code :</a>
                    <input type="text" name="code" class="form-control" placeholder="Entrez un code pour la matière">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Professeur :</a>
                    <select class="form-select" name="professeur_id" id="professeur_id">
                        @foreach($professeurs as $professeur)
                            <option value="{{$professeur->id}}">{{$professeur->user->prenom}} {{ $professeur->user->nom}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Sous-matière de :</a>
                    <select class="form-select" name="matiere_id" id="matiere_id">
                        <option value="0"></option>
                        @foreach($matieres as $matiere)
                            <option value="{{$matiere->id}}">{{$matiere->libelle}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12">
                <a class="font-bold">Promotion : </a>
                @foreach($promotions as $promotion)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{$promotion->id}}" name="promotions[]">
                    <label class="form-check-label" for="defaultCheck1">
                        {{$promotion->libelle}}
                    </label>
                </div>
                @endforeach
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
        </div>
    </div>
</form>
@endsection