@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Afficher la matière</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('matieres.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom :</a>
                {{ $matiere->libelle }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Coefficient :</a>
                {{ $matiere->coefficient }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Code :</a>
                {{ $matiere->code }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Professeur :</a>
                {{ $matiere->professeur->user->nom }} {{ $matiere->professeur->user->prenom }}
            </div>
        </div>
        @if ($matiere->parent != null)
        <div class="col-12">
            <div class="form-group">
                <a class="font-bold">Matière :</a>
                {{ $matiere->parent->libelle}}
            </div>
        </div>
        @endif
        @if ($matiere->childrens != null)
        <div class="col-12">
            <div class="form-group">
                <a class="font-bold">Sous-Matière :</a>
                @foreach ($matiere->childrens as $c)
                    {{ $c->libelle}}
                @endforeach
            </div>
        </div>
        @endif
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Classes : </a>
                @foreach ( $matiere->promotions as $p)
                    {{$p->libelle}},
                @endforeach
            </div>
        </div>
    </div>
@endsection     