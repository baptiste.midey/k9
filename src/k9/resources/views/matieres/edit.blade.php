@extends('backend.template')
   
@section('title')
    Modifier la matière
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modification de la matière</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('matieres.index') }}">Retour </a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('matieres.update', $matiere->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Nom :</a>
                    <input type="text" name="libelle" value="{{ $matiere->libelle }}" class="form-control" placeholder="Nom">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Coefficient :</a>
                    <input type="text" name="coefficient" value="{{ $matiere->coefficient }}" class="form-control" placeholder="Coefficient">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Code :</a>
                    <input type="text" name="code" value="{{ $matiere->code }}" class="form-control" placeholder="Code">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Professeur :</a>
                    <select name="professeur" id="professeur" class="form-control">
                        @foreach($professeurs as $p)
                            <option value="{{ $p->id }}">{{ $p->user->nom }} {{ $p->user->prenom }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Sous-matière de :</a>
                    <select class="form-select" name="matiere_id" id="matiere_id">
                        <option></option>
                        @foreach($matieres as $matiere)
                            <option value="{{$matiere->id}}">{{$matiere->libelle}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <a class="font-bold">Promotions :</a>
                @foreach($promotions as $p)
                <div class="form-check">
                    @if($matiere->parent->promotions->contains($p->id))
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]" checked="true">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @else
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @endif
                </div>
                @endforeach
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-outline-primary">Envoyer</button>
            </div>
        </div>
    </form>
@endsection