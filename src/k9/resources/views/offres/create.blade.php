@extends('backend.template')
  
@section('content')
<script>
    $(document).ready(function(){
        $('input[name="pdf"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('pdf').innerHTML = fileName;
        });
    });
</script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter une nouvelle offre</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('offres.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('offres.store') }}" method="POST" enctype="multipart/form-data">
   @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Nom :</a>
                    <input type="text" name="nom" class="form-control" placeholder="Entrez un nom">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Description :</a>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Entrez une description"></textarea>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <div class="mb-3">
                        <a class="font-bold">Pdf : </a>
                        <input class="form-control" type="file" id="pdf" name="pdf" accept="application/pdf">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Entreprise :</a>
                    <select class="form-select" name="entreprise_id" id="entreprise_id">
                        <option value="0"></option>
                        @foreach($entreprises as $entreprise)
                            <option value="{{$entreprise->id}}">{{ $entreprise->nom}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Tuteur : </a>
                    <select name="tuteur" id="tuteur_id" class="form-select" disabled>
                    </select>
                </div>
            </div>
            <div class="col-12">
                <a class="font-bold">Aptitude : </a>
                @foreach($aptitudes as $a)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]">
                    <label class="form-check-label" for="defaultCheck1">
                        {{$a->libelle}}
                    </label>
                </div>
                @endforeach
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
        </div>
    </div>
</form>
@endsection
@section("script")
$(document).ready(function() {

    $('#entreprise_id').change(function () { 
        var id = $('#entreprise_id option:selected').val();
        console.log(id);
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
        });
        $.ajax({
            type: 'POST',
           
            url: "{{route("tuteur_entreprise")}}",
            dataType: "json",
            data:{
                'id':id,
            },
            success: function (data) {
                if (data) {
                    $("#tuteur_id").attr('disabled', false);
                    $('#tuteur_id').empty();
                    $.each(data, function(key, tuteur) {
                        $('select[name="tuteur"]').append('<option value="'+ tuteur.id +'">' + tuteur.user.nom+ '</option>');
                    });
                }else{
                    $('#tuteur_id').empty();
                }  
            }
        });
    });
});
@endsection