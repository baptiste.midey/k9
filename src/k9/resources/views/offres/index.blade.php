@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Offres</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success"  href="{{ route('offres.create') }}">Créer une  offre</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Tuteur</th>
                <th>Aptitude</th>
                <th>Pdf</th>
                <th>Entreprise</th>
                <th width="220px">Action</th>
            </thead>
            <tbody>
                @foreach ($offre as $o)
                <tr>
                    <td>{{ $o->id }}</td>
                    <td>{{ $o->nom }}</td>
                    <td>{{$o->description }}</td>
                    <td>
                        @if ($o->tuteur != null)
                            <p>{{ $o->tuteur->user->nom }}</p>
                        @else
                            <p>Aucun tuteur</p>
                        @endif
                    </td>
                    <td>
                        @foreach($o->aptitudes as $aptitude)
                            {{ $aptitude->libelle }} 
                        @endforeach
                    </td>
                    <td>
                        @if ($o->pdf != null)
                            <p><a class="btn btn-outline-sucess btn-pdf" href="{{url('pdf/'.$o->pdf.'')}}" target="_blank"></a></p>
                        @else
                            <p>Aucun pdf</p>
                        @endif
                    </td>
                    <td>{{ $o->entreprise->nom }}</td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('offres.edit',$o->id) }}"></a>   
                        <a class="btn btn-show col" href="{{ route('offres.show',$o->id) }}"></a>    
                        <form action="{{ route('offres.destroy',$o->id) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <button type="submit" class="btn btn-delete"></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>  
    </div>    
@endsection