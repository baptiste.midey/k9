@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Afficher l'offre</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('offres.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom :</a>
                {{ $offre->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Description :</a>
                {{ $offre->description }}
            </div>
        </div>
        @if($offre->tuteut != null)
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tuteur : </strong>
                {{ $offre->tuteur->user->nom }}
            </div>
        </div>
        @endif
        @if ($offre->pdf != null)
        <div class="col-12">
            <div class="form-group">
                <a class="font-bold">Pdf :</a>
                <a class="btn btn-outline-sucess btn-pdf" href="{{url('pdf/'.$offre->pdf.'')}}" target="_blank"></a>
            </div>
        </div>
        @endif 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Aptitude : </a>
                @foreach ( $offre->aptitudes as $a)
                    {{$a->libelle}},
                @endforeach
            </div>
        </div>
    </div>
@endsection     