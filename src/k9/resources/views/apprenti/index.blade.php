@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Apprenti</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('apprenti.create') }}">Ajouter un nouvel apprenti</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif
    <table class="table table-bordered" id="example">
        <thead>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Age</th>
            <th>Promotion voulue</th>
            <th>Actions</th>
        </thead>
        <tbody>
        @foreach ($apprentis as $apprenti)
            <tr>
                <td>{{ $apprenti->user->nom }}</td>
                <td>{{ $apprenti->user->prenom }}</td>
                <td>{{ $apprenti->age }}</td>
                <td>{{ $apprenti->promotion->libelle }}</td>
                <td class="actions">
                    {{-- <a class="btn btn-edit col" href="{{ route('apprenti.edit', $apprenti) }}"></a> --}}
                    <a class="btn btn-show col" href="{{ route('apprenti.show', $apprenti) }}"></a>
                    <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$apprenti->id}}"></button>
                    <form action="{{ route('apprenti.destroy', $apprenti) }}" method="POST" class="col">
                        @csrf
                        @method('DELETE')
                        <div class="modal fade" id="{{ "modal-".$apprenti->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$apprenti->id}}" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="{{ "modalLabel-".$apprenti->id}}">
                                        Supression Apprenti</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    Voulez-vous vraiment supprimer cet apprenti?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                    <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>

@endsection
