@extends('backend.template')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2> Afficher l'apprenti {{$apprenti->user->prenom}} {{$apprenti->user->nom}}</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('apprenti.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom:</a>
                {{ $apprenti->user->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Prénom:</a>
                {{ $apprenti->user->prenom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Mail:</a>
                {{ $apprenti->user->email }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Âge :</a>
                {{ $apprenti->age }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Date de naissance :</a>
                {{ \Carbon\Carbon::parse($apprenti->date_naissance)->format('d/m/Y') }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Code postal :</a>
                {{ $apprenti->code_postal }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Ville :</a>
                {{ $apprenti->ville }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Adresse :</a>
                {{ $apprenti->adresse }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Téléphone :</a>
                {{ $apprenti->telephone }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Formation actuelle :</a>
                {{ $apprenti->formation_actuelle }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Diplôme :</a>
                {{ $apprenti->diplome }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Spécialité au lycée :</a>
                {{ $apprenti->specialite_lycee }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">LV1 :</a>
                {{ $apprenti->langue_vivante_1 }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">LV2 :</a>
                {{ $apprenti->langue_vivante_2 }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Promotion souhaitée :</a>
                {{$apprenti->promotion->libelle}}
            </div>
        </div>
    </div>
@endsection
