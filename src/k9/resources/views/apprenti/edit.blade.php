@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>
                    @isset($apprenti)
                        Modifier l'apprenti {{ $apprenti->user->prenom . ' ' . $apprenti->user->nom }}
                    @else
                        Créer l'apprenti
                    @endisset
                    </h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('apprenti.index') }}"> Retour</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <a class="font-bold">Oups! </a> Il y a eu des problèmes avec votre entrée.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @isset($apprenti)
        <form action="{{ route('apprenti.update', $apprenti) }}" method="POST">
            @method('PUT')
        @else
            <form action="{{ route('apprenti.store') }}" method="POST">
            @endisset
            @csrf
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Nom :</a>
                        <input type="text" id="nom" name="nom"
                            value="{{ !empty($apprenti) ? $apprenti->user->nom : old('nom') }}" class="form-control"
                            placeholder="Saisir un nom">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Prénom :</a>
                        <input type="text" id="prenom" name="prenom"
                            value="{{ !empty($apprenti) ? $apprenti->user->prenom : old('prenom') }}"
                            class="form-control" placeholder="Saisir un prénom">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Adresse mail :</a>
                        <input type="text" id="email" name="email"
                            value="{{ !empty($apprenti) ? $apprenti->user->email : old('email') }}" class="form-control"
                            placeholder="Saisir une adresse mail">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Date de naissance :</a>
                        <input type="date" id="date_naissance"
                            value="{{ !empty($apprenti) ? $apprenti->date_naissance : ( empty(old('date_naissance')) ? Carbon\Carbon::now()->format('Y-m-d') : old('date_naissance')) }}"
                            name="date_naissance" class="form-control"
                            data-date-start-date="d">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Téléphone :</a>
                        <input type="text" id="telephone" name="telephone"
                            value="{{ !empty($apprenti) ? $apprenti->telephone : old('telephone') }}"
                            class="form-control" placeholder="Saisir un numéro de téléphone">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Adresse :</a>
                        <input type="text" id="adresse" name="adresse"
                            value="{{ !empty($apprenti) ? $apprenti->adresse : old('adresse') }}" class="form-control"
                            placeholder="Saisir une adresse">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Code postal :</a>
                        <input type="text" id="code_postal" name="code_postal"
                            value="{{ !empty($apprenti) ? $apprenti->code_postal : old('code_postal') }}"
                            class="form-control" placeholder="Saisir un code postal">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Ville :</a>
                        <input type="text" id="ville" name="ville"
                            value="{{ !empty($apprenti) ? $apprenti->ville : old('ville') }}" class="form-control"
                            placeholder="Saisir une ville">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Formation actuelle :</a>
                        <input type="text" id="formation_actuelle" name="formation_actuelle"
                            value="{{ !empty($apprenti) ? $apprenti->formation_actuelle : old('formation_actuelle') }}"
                            class="form-control" placeholder="Saisir la formation actuel">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Diplôme :</a>
                        <input type="text" id="diplome" name="diplome"
                            value="{{ !empty($apprenti) ? $apprenti->diplome : old('diplome') }}" class="form-control"
                            placeholder="Saisir un diplôme">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">Spécialitée choisie au lycée :</a>
                        <input type="text" id="specialite_lycee" name="specialite_lycee"
                            value="{{ !empty($apprenti) ? $apprenti->specialite_lycee : old('specialite_lycee') }}"
                            class="form-control" placeholder="Saisir la spécialiser que l'élève a choisi au lycée">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">LV1 :</a>
                        <input type="text" id="langue_vivante_1" name="langue_vivante_1"
                            value="{{ !empty($apprenti) ? $apprenti->langue_vivante_1 : old('langue_vivante_1') }}"
                            class="form-control" placeholder="Saisir la LV1">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <a class="font-bold">LV2 :</a>
                        <input type="text" id="langue_vivante_2" name="langue_vivante_2"
                            value="{{ !empty($apprenti) ? $apprenti->langue_vivante_2 : old('langue_vivante_2') }}"
                            class="form-control" placeholder="Saisir la LV2">
                    </div>
                </div>
                <label for="promotion_id" class="font-bold">Promotion souhaitée :</label>
                <div class="col-12">
                    <div class="form-group">
                        @foreach ($promotions as $promotion)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="{{ $promotion->id }}"
                                    name="promotion_id" id="promotion_id"
                                    {{ !empty($apprenti) ? ($apprenti->promotion->id == $promotion->id ? 'Checked' : '') : '' }}>
                                <label class="form-check-label">{{ $promotion->libelle }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </div>
            </div>
        </form>
    @endsection
