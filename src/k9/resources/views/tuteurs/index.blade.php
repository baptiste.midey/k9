@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Tuteurs</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('tuteurs.create') }}">Ajouter une nouveau tuteur</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>Prenom</th>
                <th>Nom</th>
                <th>Email</th>
                <th>Tel.</th>
                <th>Entreprise</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
            @foreach ($tuteurs as $tuteur) 
                <tr>
                  
                    <td>{{ $tuteur->user->prenom }}</td>
                    <td>{{ $tuteur->user->nom }}</td>
                    <td>{{ $tuteur->user->email }}</td>
                    <td>{{ $tuteur->telephone }}</td>
                    <td>
                        <a class="btn" href="{{ route('entreprises.show', $tuteur->entreprise) }}" style="width: auto"> {{ $tuteur->entreprise->nom }}</a>
                    </td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('tuteurs.edit', $tuteur) }}"></a>
                        <a class="btn btn-show col" href="{{ route('tuteurs.show', $tuteur) }}"></a>
                             <!-- Button trigger modal -->
                    <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$tuteur->id}}"></button>
                    
                    <form action="{{ route('tuteurs.destroy',$tuteur) }}" method="POST" >
                        @csrf
                        @method('DELETE')
                    <!-- Modal -->
                    <div class="modal fade" id="{{ "modal-".$tuteur->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$tuteur->id}}" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="{{ "modalLabel-".$tuteur->id}}">
                                Supression Tuteur</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            Voulez-vous vraimment supprimer ce tuteur?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
