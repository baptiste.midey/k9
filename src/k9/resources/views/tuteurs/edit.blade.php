@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>{{ !empty($tuteur) ? 'Modifier' : 'Créer' }} un tuteur</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('tuteurs.index') }}">Retour</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            Une erreur est survenue..<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @isset($tuteur)
        <form action="{{ route('tuteurs.update', $tuteur) }}" method="POST">
            @method('PUT')
            @else
            <form action="{{ route('tuteurs.store') }}" method="POST">
            @endisset
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Nom:</a>
                            <input type="text" name="nom" class="form-control" placeholder="Dupont"
                                value="{{ !empty($tuteur) ? $tuteur->user->nom : old('nom') }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Prénom:</a>
                            <input class="form-control" name="prenom" placeholder="Jean"
                                value="{{ !empty($tuteur) ? $tuteur->user->prenom : old('prenom') }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Adresse mail:</a>
                            <input type="text" name="email" id="email" class="form-control"
                                placeholder="JeanDupont@gmail.com"
                                value="{{ !empty($tuteur) ? $tuteur->user->email : old('email') }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Téléphone:</a>
                            <input type="text" name="telephone" id="telephone" class="form-control"
                                placeholder="03 81 01 23 45"
                                value="{{ !empty($tuteur) ? $tuteur->telephone : old('telephone') }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <a class="font-bold">Entreprise:</a>
                            <select class="form-select" name="entreprise_id" id="entreprise_id">
                                @foreach ($entreprises as $entreprise)
                                    <option value="{{ $entreprise->id }}"
                                        {{ !empty($tuteur) ? ($tuteur->entreprise->id == $entreprise->id ? 'selected' : '') : '' }}>
                                        {{ $entreprise->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <a class="font-bold">Apprenti:</a>
                                    <select class="form-select" name="apprenti_id" id="apprenti_id">
                                        <option value="" {{ !empty($tuteurApprentis) ? 'selected' : '' }}>Aucun apprenti
                                        </option>
                                        @foreach ($apprentis as $apprenti)
                                            <option value="{{ $apprenti->id }}" @if (!empty($tuteur) && !empty($tuteurApprentis))
                                                @foreach ($tuteurApprentis as $ta)
                                                    @if ($loop->first)
                                                     Selected
                                                    @endif
                                                @endforeach
                                        @endif
                                        >{{ $apprenti->user->nom ." ". $apprenti->user->prenom}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <a class="font-bold">Date de début :</a>
                                        <input type="date" name="apprenti_debut" id="apprenti_debut" class="form-control"
                                        @if(!empty($tuteurApprentis))
                                                @foreach ($tuteurApprentis as $ta)
                                                    @if ($loop->first)
                                                     value="{{$ta->pivot->date_debut}}"
                                                    @endif
                                                @endforeach
                                        @else
                                        value="{{Carbon\Carbon::now()->format('Y-m-d')}}"
                                        @endif
                                        >
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <a class="font-bold">Date de fin:</a>
                                        <input type="date" name="apprenti_fin" id="apprenti_fin" class="form-control"
                                        @if(!empty($tuteurApprentis))
                                                @foreach ($tuteurApprentis as $ta)
                                                    @if ($loop->first)
                                                     value="{{$ta->pivot->date_fin}}"
                                                    @endif
                                                @endforeach
                                        @else
                                        value="{{Carbon\Carbon::now()->format('Y-m-d')}}"
                                        @endif
                                        >
                                    </div>
                                </div>
                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    @endsection
