@extends('template')
<div class="container"></div>
<x-auth-card>
    <x-slot name="logo"></x-slot>

    <!-- Validation Errors -->
    <x-auth-validation-errors class="mb-4" :errors="$errors" />
    <div class="col-md-6 offset-md-3 login">
        <div class="card" style="margin-top: 100px;">
            <h5 class="card-header">Changement de mot de passe</h5>
            <div class="card-body">

                <form method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <!-- Password Reset Token -->
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">

                    <!-- Email Address -->
                    {{-- <div>
                            <x-label for="email" :value="__('Email')" /> --}}

                    <x-input id="email" class="block mt-1 w-full" type="hidden" name="email"
                        :value="old('email', $request->email)" required autofocus />
                    {{-- </div> --}}

                    <!-- Password -->
                    <div class="mt-4">
                        <x-label for="password" :value="__('Password')" />

                        <x-input id="password" class="block mt-1 w-full" type="password" name="password" required />
                    </div>

                    <!-- Confirm Password -->
                    <div class="mt-4">
                        <x-label for="password_confirmation" :value="__('Confirm Password')" />

                        <x-input id="password_confirmation" class="block mt-1 w-full" type="password"
                            name="password_confirmation" required />
                    </div>

                    <div class="flex items-center justify-end mt-4 btn-login">
                        <x-button>
                            {{ __('Reset Password') }}
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-auth-card>

</div>
