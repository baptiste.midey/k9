@extends('template')
<div class="container">
    <x-auth-card>
        <x-slot name="logo"></x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="col-md-6 offset-md-3 login">
            <div class="card" style="margin-top: 100px;">
                <h5 class="card-header">Connexion</h5>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <!-- Email Address -->
                        <div>
                            <x-label for="email" :value="__('email')" />

                            <x-input id="email" class="block mt-1 w-full" type="email" name="email" required autofocus />
                        </div>
                        <!-- Password -->
                        <div class="mt-4">
                            <x-label for="password" :value="__('Password')" />

                            <x-input id="password" class="block mt-1 w-full" type="password" name="password" required
                                autocomplete="current-password" />
                        </div><!-- Remember Me -->
                        <div class="block mt-4">
                            <label for="remember_me" class="inline-flex items-center">
                                <input id="remember_me" type="checkbox"
                                    class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                    name="remember">
                                <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                            </label>
                        </div>

                            {!! GoogleReCaptchaV3::renderField('login_id', 'login_action') !!}

                        <div class="flex items-center justify-end mt-4">
                            @if (Route::has('password.request'))
                                <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                    href="{{ route('password.request') }}">
                                    {{ __('Forgot your password?') }}
                                </a>
                            @endif

                            <x-button class="ml-3 btn-login">
                                {{ __('Log in') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </x-auth-card>
</div>
<style>
    label {
        min-width: 150px;
    }

    .login .btn-login {
        /* text-align: center; */
        padding: 10px 25px;
        width: 25%;
        margin-left: 41.5%;
        /* bottom: 10px; */
        margin-top: 25px;
        color: black !important;
        border-radius: 0.25rem;
        background-color: transparent;
        text-transform: uppercase;
        transition: background-color 350ms linear;
    }

</style>
