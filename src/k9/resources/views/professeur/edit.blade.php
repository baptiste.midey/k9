@extends('backend.template')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modifier le professeur</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('professeur.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            Une erreur est survenue..<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('professeur.update', $professeur) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Nom:</strong>
                    <input type="text" name="nom" id="nom" class="form-control" placeholder="Name" value="{{$professeur->user->nom}}">
                </div>
                <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <a class="font-bold">Prénom:</strong>
                    <input type="text" name="prenom" id="prenom" class="form-control" placeholder="prenom" value="{{$professeur->user->prenom}}">
                </div>
                <div class="form-group">
                    <a class="font-bold">Adresse</strong>
                    <input type="text" name="email" id="email" class="form-control" placeholder="adresse" value="{{$professeur->user->email}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <a class="font-bold">Promotion : </strong>
                @foreach($promotions as $p )
                <div class="form-check">
                    @if($professeur->promotions->contains($p->id))
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]" checked="true">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @else
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    @endif
                </div>
                @endforeach
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Valider</button>
            </div>
        </div>
   
    </form>
@endsection