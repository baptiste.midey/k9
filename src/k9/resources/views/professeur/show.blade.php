@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Professeur</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('professeur.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom :</a>
                {{ $professeur->user->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Prenom :</a>
                {{ $professeur->user->prenom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Adresse mail :</a>
                {{ $professeur->user->email }}
            </div>
        </div>
      
    </div>
@endsection     