@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Professeurs</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('professeur.create') }}">Créer un nouveau professeur</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif
<div class="padd20">
    <table class="table table-bordered" id="example">
        <thead>
            <th>ID</th>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Email</th>
            <th>Promotion(s) liée(s)</th>
            <th width="280px">Action</th>
        </thead>
        <tbody>   
            @foreach ($professeurs as $professeur)
            <tr>
            <td>{{ $professeur->id }}
                <td>{{ $professeur->user->nom }}</td>
                <td>{{ $professeur->user->prenom }}</td>
                <td>{{ $professeur->user->email }}</td>
                <td>
                    @foreach($professeur->promotions as $p)
                    {{ $p->libelle }},
                    @endforeach
                </td>
                <td class="actions">
                    <a class="btn btn-edit col" href="{{ route('professeur.edit', $professeur) }}"></a>
                    <a class="btn btn-show col" href="{{ route('professeur.show', $professeur) }}"></a>
                    
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$professeur->id}}"></button>
                    
                    <form action="{{ route('professeur.destroy',$professeur) }}" method="POST" >
                        @csrf
                        @method('DELETE')
                    <!-- Modal -->
                    <div class="modal fade" id="{{ "modal-".$professeur->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$professeur->id}}" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="{{ "modalLabel-".$professeur->id}}">
                                Supression professeur</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            Voulez-vous vraimment supprimer ce professeur?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody>     
    </table>
</div>
@endsection