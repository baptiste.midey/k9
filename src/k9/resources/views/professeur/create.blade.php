@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter un professeur</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('professeur.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        Une erreur est survenue..<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('professeur.store') }}" method="POST">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Nom:</a>
                    <input type="text" name="nom" class="form-control" placeholder="nom">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Prenom:</a>
                    <input class="form-control" name="prenom" placeholder="prenom">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Adresse mail</a>
                    <input type="text" name="email" id="email" class="form-control" placeholder="adresse">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <a class="font-bold">Sélectionner la ou les promotions de l'enseignant :</a>
                    @foreach($promotions as $p)
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="{{$p->id}}" name="promotions[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$p->libelle}}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>    
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Valider</button>
            </div>
        </div>
    </div>
</form>
@endsection