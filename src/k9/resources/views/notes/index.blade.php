@extends('backend.professeur')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Notes</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success"  href="{{ route('notes.create') }}">Ajoutée une note</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>Eleve</th>
                <th>Promotion</th>
                <th>Matière</th>
                <th>Date</th>
                <th>Coefficient</th>
                <th>Notes</th>
                <th width="220px">Action</th>
            </thead>
            <tbody>
                @foreach ($notes as $note)
                <tr>
                    <td>{{ $note->apprenti->user->nom }} {{ $note->apprenti->user->prenom }}</td>
                    <td>{{ $note->apprenti->promotion->libelle }}</td>
                    <td>{{ $note->matiere->libelle }}</td>
                    <td>{{ $note->date }}</td>
                    <td>{{ $note->coef}}</td>
                    <td>{{ $note->note }}</td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('notes.edit',$note) }}"></a>  
                        <form action="{{ route('notes.destroy',$note) }}" method="POST" class="col">   
                            @csrf
                            @method('DELETE')      
                            <button type="submit" class="btn btn-delete"></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>  
    </div>    
@endsection