@extends('backend.professeur')

@section('title')
    @isset($note)
        Modifier la note
    @else
        Ajoutée une note
    @endisset
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                    <h2>Modification de la note</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('notes.index') }}">Retour </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <form action="{{ route('notes.update', $note) }}" method="POST">
            @method('PUT')

            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <a class="font-bold">Promotion :</a>
                        <select name="promotion_id" id="promotion_id" class="form-control">
                            @foreach ($promotions as $p)
                                <option value="{{ $p->id }}"
                                        @if($note->promotion_id == $p->id)
                                            selected
                                        @endif
                                    >{{ $p->libelle }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <a class="font-bold">Matière :</a>
                        <select name="matiere_id" id="matiere_id" class="form-control">
                                        @foreach($matieresResult as $m)
                                            <option value="{{ $m->id }}"
                                                @if($note->matiere_id == $p->id)
                                                    selected
                                                @endif
                                            >{{ $m->libelle }}</option>
                                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <a class="font-bold">Période :</a>
                        <select name="periode_id" id="periode_id" class="form-control">
                                        @foreach($periodes as $p)
                                            <option value="{{ $p->id }}"
                                                @if($note->periode_id == $p->id)
                                                    selected
                                                @endif
                                            >{{ $p->annee }}</option>
                                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <a class="font-bold">Elève :</a>
                        <select name="eleve_id" id="eleve_id" class="form-control">
                                        @foreach($apprentis as $a)
                                            <option value="{{ $a->id }}"
                                                @if($note->periode_id == $p->id)
                                                    selected
                                                @endif
                                            >{{ $a->user->nom }} {{ $a->user->prenom }}</option>
                                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <a class="font-bold">Coefficient :</a>
                        <input type="text" id="coefficient" name="coefficient"
                            value="{{$note->coef}}"
                            class="form-control"
                            placeholder="Saisir un coefficient">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <a class="font-bold">Note :</a>
                    <input type="text" id="note" name="note" 
                            value="{{$note->note}}" 
                        class="form-control" placeholder="Saisir une note">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-outline-primary">Envoyer</button>
                </div>
            </div>
        </form>
    @endsection
    @section('script')
            $(document).ready(function() {
                $('#promotion_id').change(function() {
                    try{
                        getMatieres();
                    }catch(e){}
                    try{
                        getApprentis();
                    }catch(e){}
                    try{
                        getPeriodes();
                    }catch(e){}
                });
            });

            function getMatieres() {
                var id = $('#promotion_id option:selected').val();
                if(id){
                    var url = '{{ route('notes.create.matieres', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        success: function(data) {
                            console.log("matieres :");
                            console.log(data);
                            $('#matiere_id').html('');
                            data.forEach(matiere => {
                                $('#matiere_id').html('<option value="' + matiere.id + '">' + matiere.libelle +
                                    '</option>');
                            });
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            $('#matiere_id').html('');
                            console.log("Error :");
                            console.log(xhr.responseText);
                        }
                    });
                }
            }

            function getPeriodes() {
                var id = $('#promotion_id option:selected').val();
                if(id){
                    var url = '{{ route('notes.create.periodes', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        success: function(data) {
                            console.log("periodes :");
                            console.log(data);
                            $('#periode_id').html('');
                            data.forEach(periode => {
                                $('#periode_id').html('<option value="' + periode.id + '">' + periode.annee +
                                    '</option>');
                            });
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            $('#periode_id').html('');
                            console.log("Error :");
                            console.log(xhr.responseText);
                        }
                    });
                }
            }

            function getApprentis() {
                var id = $('#promotion_id option:selected').val();
                if(id){
                    var url = '{{ route('notes.create.apprentis', ':id') }}';
                    url = url.replace(':id', id);
                    $.ajax({
                        type: 'GET',
                        url: url,
                        success: function(data) {
                            console.log("apprentis :");
                            console.log(data);
                            $('#eleve_id').html('');
                            data.forEach(apprenti => {
                                $('#eleve_id').html('<option value="' + apprenti.id + '">' + apprenti.user
                                    .nom + ' ' + apprenti.user.prenom + '</option>');
                            });
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            $('#eleve_id').html('');
                            console.log("Error :");
                            console.log(xhr.responseText);
                        }
                    });
                }
            }
    @endsection
