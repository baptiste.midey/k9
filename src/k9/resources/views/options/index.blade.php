@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Options</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('options.create') }}"> Créer une nouvelle option</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Libelle</th>
                <th>Promotion</th>
                <th width="280px">Action</th>
            </thead>
            <tbody>
                @foreach ($options as $o)
                <tr>
                    <td>{{ $o->id }}</td>
                    <td>{{ $o->libelle }}</td>
                    <td>{{$o->promotion->libelle}}</td>
                    <td class="actions">
                    <a class="btn btn-edit col" href="{{ route('options.edit',$o->id) }}"></a>   
                    <a class="btn btn-show col" href="{{ route('options.show',$o->id) }}"></a>    
                               <!-- Button trigger modal -->
                    <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$o->id}}"></button>
                    
                        <form action="{{ route('options.destroy',$o) }}" method="POST" >
                        @csrf
                        @method('DELETE')
                        <!-- Modal -->
                        <div class="modal fade" id="{{ "modal-".$o->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$o->id}}" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="{{ "modalLabel-".$o->id}}">
                                    Supression Option</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                Voulez-vous vraiment supprimer cette option?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>
    </div>    
@endsection
