@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2> Afficher l'option</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('options.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Nom de l'option :</a>
                {{ $option->libelle }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Promotion associée :</a>
                {{ $option->promotion->libelle }}
            </div>
        </div>
    </div>
@endsection     