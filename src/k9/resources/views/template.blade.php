<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link  rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- CSS only -->
    <script>
        window.axeptioSettings = {
          clientId: "60ddad4cb221bd759ed3f2a0",
          cookiesVersion: "scrum-base",
        };
         
        (function(d, s) {
          var t = d.getElementsByTagName(s)[0], e = d.createElement(s);
          e.async = true; e.src = "//static.axept.io/sdk.js";
          t.parentNode.insertBefore(e, t);
        })(document, "script");
    </script>
    
</head>
<body>
    <header>
        <section id="header">
            <nav class="navigation">
                <div class="logo">
                    <a href="{{route('welcome')}}">
                        <img src="{{ url('img/ora-final.png')}}" alt="logo lycée">
                    </a>
                </div>
                <div class="nav">
                   <ul>
                        @auth
                        <li><a href="{{ route('welcome') }}" class="text-sm text-gray-700 underline">Accueil</a></li>
                            @if ( Auth::user()->isAdmin == true) 
                                <li><a href="{{ url('/admin') }}" class="text-sm text-gray-700 underline">Dashboard</a></li>
                            @endif
                            @if ( Auth::user()->profil_type == "App\Models\Professeur")
                            <li><a href="{{route('profdashboard')}}" class="text-sm text-gray-700 underline">Mon compte</a></li>
                            @endif
                            <li>
                                <a href="{{ route('profil') }}" class="text-sm text-gray-700 underline">Mon profil</a>
                            </li>
                            @if ( Auth::user()->profil_type == "App\Models\Entreprise") 
                            <li>
                                <a href="{{ route('offre_entreprise.index') }}" class="text-sm text-gray-700 underline">Mes offres</a>
                            </li>
                            @endif
                            <li>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a href="{{ route('logout') }}" class="text-sm text-gray-700 underline">
                                        <button class="btn" type="submit">Déconnexion</button>
                                    </a>
                                </form>
                            </li>
                        @endauth
                   </ul>
                </div>
            </nav>
        </section>
    </header>
    <section  id="profil" style="padding:120px;">
        <div class="container">
            @yield('content')
        </div>
    </section>
    <footer>
        <a href="{{route('mentions')}}" target="_blank">Mentions Légales</a>&copy; Organisation de recrutement des apprentis
    </footer>
    
    {!!  GoogleReCaptchaV3::init() !!}
    
</body>