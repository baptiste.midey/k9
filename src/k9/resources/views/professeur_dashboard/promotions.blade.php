@extends('backend.professeur')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Mes Promotions</h2>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif

    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Libelle</th>
                <th>Date de début</th>
                <th>Date de fin</th>
            </thead>
            <tbody>
            @foreach ($promotions as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->libelle }}</td>
                    <td>{{\Carbon\Carbon::parse($p->date_debut)->format('d/m/Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($p->date_fin)->format('d/m/Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

