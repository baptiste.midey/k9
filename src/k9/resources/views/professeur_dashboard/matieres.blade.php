@extends('backend.professeur')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Mes Matières</h2>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>ID</th>
                <th>Nom</th>
                <th>Coefficient</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($matieres as $m)
                <tr>
                    <td>{{ $m->id }}</td>
                    <td>{{ $m->libelle }}</td>
                    <td>{{ $m->coefficient }}</td>
                    <td>{{ $m->code}}</td>
                </tr>
                @endforeach
            </tbody>    
        </table>  
    </div>
@endsection