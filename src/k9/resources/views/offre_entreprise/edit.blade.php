@extends('template')
   
@section('title')
    Modifier offre
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modification de l'offre</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('offre_entreprise.index') }}">Retour </a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('offre_entreprise.update', $offre->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="nom">Nom :</label>
                    <input type="text" name="nom" value="{{ $offre->nom }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="description">Description :</label>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Detail">{{ $offre->description }}</textarea>
                </div>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tuteur : </strong>
                    <select name="tuteur" id="tuteur" class="form-control">
                        @foreach($tuteur as $t)
                            <option value="{{ $t->id }}">{{ $t->user->nom }}</option>
                        @endforeach
                    </select>
                </div>
            </div> --}}
            <div class="col-12">
                <div class="form-group">
                    <div class="mb-3">
                        <label for="pdf">Nom :</label>
                        <input class="form-control" type="file" value="{{$offre->pdf}}" id="pdf" name="pdf" accept="application/pdf">
                    </div>
                </div>
            </div>
            @if ($offre->pdf != null) 
            <div class="col-12">
                <div class="form-group">
                    <label for="pdf">Pdf actuel :</label>
                    <a href="{{url('pdf/'.$offre->pdf.'')}}" target="_blank">{{$offre->pdf}}</a>
                </div>
            </div>
            @endif  
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label for="aptitude">Aptitude :</label>
                @foreach($aptitudes as $a )
                <div class="form-check">
                    @if($offre->aptitudes->contains($a->id))
                        <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]" checked="true">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$a->libelle}}
                        </label>
                    @else
                        <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$a->libelle}}
                        </label>
                    @endif
                </div>
            @endforeach
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-outline-primary">Envoyer</button>
            </div>
        </div>
    </form>
@endsection