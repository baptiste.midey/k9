@extends('template')
  
@section('content')
<script>
    $(document).ready(function(){
        $('input[name="pdf"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('pdf').innerHTML = fileName;
        });
    });
</script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter une nouvelle offre</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('offre_entreprise.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('offre_entreprise.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nom">Nom :</label>
                    <input type="text" name="nom" class="form-control" placeholder="Entrez un nom">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="description">Description :</label>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Entrez une description"></textarea>
                </div>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tuteur : </strong>
                    <select name="tuteur" id="tuteur" class="form-control">
                            @foreach($tuteur as $t)
                                <option value="{{ $t->id }}">{{ $t->user->nom }}</option>
                            @endforeach
                    </select>
                </div>
            </div> --}}
            <div class="col-12">
                <div class="form-group">
                    <div class="mb-3">
                        <label for="pdf">Pdf :</label>
                        <input class="form-control" type="file" id="pdf" name="pdf" accept="application/pdf">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="entreprise">Entreprise :</label>
                    <select class="form-select" name="entreprise_id">
                        @foreach($entreprises as $entreprise)
                            <option value="{{$entreprise->id}}">{{ $entreprise->nom}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12">
                <label for="aptitude">Aptitudes :</label>
                @foreach($aptitudes as $a)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]">
                    <label class="form-check-label" for="defaultCheck1">
                        {{$a->libelle}}
                    </label>
                </div>
                @endforeach
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
        </div>
    </div>
</form>
@endsection
