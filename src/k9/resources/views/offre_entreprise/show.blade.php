@extends('template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Afficher l'offre</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('offre_entreprise.index') }}">Retour</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="nom">Nom :</label>
                {{ $offre->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="description">Description :</label>
                {{ $offre->description }}
            </div>
        </div>
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tuteur : </strong>
                {{ $offre->tuteur->user->nom }}
            </div>
        </div> --}}
        @if ($offre->pdf != null)
        <div class="col-12">
            <div class="form-group">
                <label for="pdf">Pdf :</label>
                <a href="{{url('pdf/'.$offre->pdf.'')}}" target="_blank">{{$offre->pdf}}</a>
            </div>
        </div>
        @endif 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="aptitude">Aptitude :</label>
                @foreach ( $offre->aptitudes as $a)
                    {{$a->libelle}},
                @endforeach
            </div>
        </div>
    </div>
@endsection     