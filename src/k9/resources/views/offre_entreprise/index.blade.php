@extends('template')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Mes Offres</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-success"  href="{{ route('offre_entreprise.create') }}">Créer une nouvelle offre</a>
        </div>
    </div>
</div>
<div class="row">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    @if($errors->any())
        <div class="alert alert-danger">
            <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
<div class="padd20">
    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Nom</th>
            <th>Description</th>
            {{-- <th>Tuteur</th> --}}
            <th>Aptitude</th>
            <th>Pdf</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($offres as $o)
            <tr>
                <td>{{ $o->id }}</td>
                <td>{{ $o->nom }}</td>
                <td>{{ $o->description }}</td>
                <td>
                    @foreach($o->aptitudes as $aptitude)
                    {{ $aptitude->libelle }} 
                    @endforeach
                </td>
                <td>@if ($o->pdf != null)
                    <p><a class="btn btn-secondary btn-pdf" href="{{url('pdf/'.$o->pdf.'')}}" target="_blank">Pdf</a></p>
                    @else
                    <p>Pas de pdf renseigné</p>
                    @endif
                </td>
                <td class="actions" style="width:270px;">
                    <div class="bouton" style="display: flex;flex-direction: row;flex-wrap: nowrap;justify-content: space-between;">
                        <a class="btn btn-info " href="{{ route('offre_entreprise.show',$o->id) }}">Voir</a>    
                        <a class="btn btn-warning " href="{{ route('offre_entreprise.edit',$o->id) }}">Modifier</a>   
                        <form action="{{ route('offre_entreprise.destroy',$o->id) }}" method="POST" class="">   
                            @csrf
                            @method('DELETE')      
                            <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>    
    </table>  
</div>    
{{-- <form action="" method="POST">
    @csrf
</form> --}}
@endsection