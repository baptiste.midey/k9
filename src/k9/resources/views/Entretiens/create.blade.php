@extends('backend.template')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter un nouvel entretien</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('entretiens.index') }}">Retour </a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('entretiens.store') }}" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <a class="font-bold">Date d'entretien :</a>
            <input type="date" name="date_entretien" class="form-control" value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <a class="font-bold">Décision :</a>
            <select class="form-select" name="decision">
              <option value="2">Accepté</option>
              <option value="1">En cours</option>
              <option value="0">Refusé</option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <a class="font-bold">Responsable de dispositif :</a>
            <select class="form-select" name="rd_id">
              @foreach($rds as $rd)
                <option value="{{$rd->id}}">{{ $rd->user->nom . " " . $rd->user->prenom}} </option>
              @endforeach
          </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <a class="font-bold">Apprenti :</a>
            <select class="form-select" name="apprenti_id">
              @foreach($apprentis as $apprenti)
                <option value="{{$apprenti->id}}">{{ $apprenti->user->nom . " " . $apprenti->user->prenom}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <a class="font-bold">Promotion :</a>
            <select class="form-select" name="promotion_id">
                @foreach ($promotions as $p )
                    <option value="{{$p->id}}">{{ $p->libelle }}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Envoyer</button>
        </div>
    </div>
   
</form>
@endsection