@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2> Afficher l'entretien</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('entretiens.index') }}">Retour </a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <a class="font-bold">Date :</a>
                {{ $entretien->date_entretien }}
            </div>
            <div class="form-group">
                <a class="font-bold">Décision :</a>
                {{ $entretien->decision ? "Accepté" : "Refusé" }}
            </div>
            <div class="form-group">
                <a class="font-bold">Responsable de dispositif concerné :</a>
                {{ $entretien->rd->user->prenom .' '.$entretien->rd->user->nom }}
            </div>
            <div class="form-group">
                <a class="font-bold">Apprenti concerné :</a>
                {{ $entretien->apprenti->user->prenom.' '.$entretien->apprenti->user->nom }}
            </div>
            <div class="form-group">
                <a class="font-bold">Promotion concernée :</a>
                {{ $entretien->promotion->libelle }}
            </div>
        </div>
    </div>
@endsection