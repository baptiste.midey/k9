@extends('backend.template')

@section('title')
    Modifier entretien
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modifier l'entretien</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('entretiens.index') }}"> Retour</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            Une erreur est survenue..<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form method="POST" action="{{ route('entretiens.update', $entretien) }}">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Date d'entretien :</strong>
                    <input type="date" name="date_entretien" class="form-control"value="{{ $entretien->date_entretien }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Décision :</strong>
                    <select class="form-select" name="decision">
                        <option value="2" @if ($entretien->decision)
                            selected="selected"
                            @endif
                            >Accepté
                        </option>
                        <option value="1" @if (!$entretien->decision)
                            selected="selected"
                            @endif
                            >Refusé
                        </option>
                        <option value="0" @if ($entretien->decision)
                            selected="selected"
                            @endif
                            >En cours
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Responsable de dispositif :</strong>
                    <select class="form-select" name="rd_id">
                        @foreach ($rds as $rd)
                            <option value="{{ $rd->id }}" 
                                @if ($rd->id == $entretien->rd->id)
                                selected="selected"
                                @endif
                            >{{ $rd->user->nom . ' ' . $rd->user->prenom }} 
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apprenti :</strong>
                    <select class="form-select" name="apprenti_id">
                        @foreach ($apprentis as $apprenti)
                            <option value="{{ $apprenti->id }}" @if ($apprenti->id == $entretien->apprenti->id)
                                selected="selected"
                        @endif
                        >{{ $apprenti->user->nom . ' ' . $apprenti->user->prenom }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Promotion :</strong>
                    <select class="form-select" name="promotion_id">
                        @foreach ($promotions as $promotion)
                            <option value="{{ $promotion->id }}" @if ($promotion->id == $entretien->promotion->id)
                                selected="selected"
                        @endif
                        >{{ $promotion->libelle }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
        </div>

    </form>
@endsection
