@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Entretiens</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('entretiens.create') }}"> Créer un Entretien</a>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('fiches.create') }}"> Remplir une fiche</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($errors->any())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>
    @endif
   
    <div class="padd20">
        <table class="table table-bordered" id="example">
            <thead>
                <th>Date d'entretien</th>
                <th>Decision</th>
                <th>RD</th>
                <th>Eleve</th>
                <th>Promotion</th>
                <th>Fiche</th>
                <th width="280px">Action</th>
            </thead> 
            <tbody>   
                @foreach ($entretiens as $e)
                <tr>
                    <td>{{\Carbon\Carbon::parse($e->date_entretien)->format('d/m/Y')}}</td>
                    @if ($e->decision == 2)
                    <td>Accepté</td>
                    @elseif ($e->decision == 1)
                    <td>En cours</td>
                    @elseif ($e->decision == 0)
                    <td>Refusé</td>
                    @endif
                    <td>{{$e->rd->user->prenom . " " . $e->rd->user->nom}}</td>
                    <td>{{$e->apprenti->user->prenom . " " . $e->apprenti->user->nom}}</td>
                    <td>{{$e->promotion->libelle}}</td>
                    <td>@if ($e->fiche_id != null)
                        <p><a class="btn btn-outline-sucess btn-pdf" href="{{ route('getPostPdf',$e->fiche_id) }}"></a></p>
                        @else
                        <p>Pas de fiche créée</p>
                        @endif
                    </td>
                    <td class="actions">
                        <a class="btn btn-edit col" href="{{ route('entretiens.edit',$e->id) }}"></a>
                        <a class="btn btn-show col" href="{{ route('entretiens.show',$e->id) }}"></a>
                        <button type="button" class="btn btn-delete col" data-bs-toggle="modal" data-bs-target="#{{ "modal-".$e->id}}"></button>
                        <form action="{{ route('entretiens.destroy',$e->id) }}" method="POST" class="col">
                            @csrf
                            @method('DELETE')
                           <!-- Modal Suppression-->
                            <div class="modal fade" id="{{ "modal-".$e->id}}" tabindex="-1" aria-labelledby="{{ "modalLabel-".$e->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="{{ "modalLabel-".$e->id}}">
                                            Supression Entretien</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        Voulez-vous vraiment supprimer cet entretien?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn-modal btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                        <button type="submit" class="btn-modal btn-primary">Supprimer</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>    
        </table>     
    </div> 
@endsection
