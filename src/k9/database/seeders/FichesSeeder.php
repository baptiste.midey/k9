<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class FichesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('fiches')->insert([
            'demandeur_emploi' => 0,
            'autre_demandeur' => 'Etudiant',
            'formation_projet' => 1,
            'connaissance' => 1,
            'motivation' => 1,
            'niveau_scolaire' => 3,
            'experience_pro' => 1,
            'experience_pro_text' => 'Stage dans des entreprises de développement',
            'atouts' => 'Persévérant,Méthodique,Calme',
            'difficultes_solutions' => 'Aucune difficultés',
            'entreprise_trouvee' => 1,
            'post_conforme' => 1,
            'demarche' => 1,
            'type_entreprise' => 'Entreprise de développement',
            'secteur_geo' => 'Jura',
            'bilan' => 'Bon bilan',
            'amenagements' => 3,
            'permis' => 1,
            'vehicule' => 1,
            'decision' => 'Le candidat est accepté dans la formation souhaitée',
            'entretien_id' => 1
        ]);
    }
}
