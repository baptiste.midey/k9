<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;


class EntretiensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('entretiens')->insert([
            'date_entretien' => Carbon::create('2021', '06', '17'),
            'decision' => 2,
            'apprenti_id' => 1,
            'rd_id' => 1,
            'promotion_id' => 2,
            'fiche_id' => 1
        ]);
        DB::table('entretiens')->insert([
            'date_entretien' => Carbon::create('2021', '09', '17'),
            'decision' => 1,
            'apprenti_id' => 2,
            'rd_id' => 2,
            'promotion_id' => 2,
            'fiche_id' => null
        ]);
    }
}
