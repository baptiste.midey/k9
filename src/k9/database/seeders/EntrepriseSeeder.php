<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntrepriseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entreprises')->insert([
            'nom' => 'IDMM',
            'adresse' => '13 rue Henry Jean Renaud 39100 Dole', 
            'raison_sociale' => 'SARL',
            'telephone' => '0384889625',
        ]);
        DB::table('entreprises')->insert([
            'nom' => 'Publipresse',
            'adresse' => '1 Rue de la Brasserie 25500 Morteau', 
            'raison_sociale' => 'SARL',
            'telephone' => '0354789652',
        ]);
        DB::table('entreprises')->insert([
            'nom' => 'DLM Soft',
            'adresse' => '10 Place Général de Gaulle 71100 Chalon-sur-Saône', 
            'raison_sociale' => 'SARL',
            'telephone' => '0347896510',
        ]);
    }
}
