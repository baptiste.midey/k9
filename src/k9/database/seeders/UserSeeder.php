<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nom' => 'root',
            'prenom' => 'root',
            'email' => 'root@gmail.com',
            'password' => '$2y$10$HZMuLm8DMUJzBDBtuuSYi.V2sGOeA/SDR7TkRKiMjIH5WfYZNb1Py',
            'isAdmin' => true,
        ]);
        DB::table('users')->insert([
            'nom' => 'tuteurIDMM',
            'prenom' => 'tuteurIDMM', 
            'email' => 'tuteurIDMM@gmail.com',
            'password' => '$2y$10$PkmiOLDhJp1k0U4AIrQJ3eyGcbF7Lb/dSRZOcQHSNykixU6bAw6x2',
            'profil_type' => 'App\Models\Tuteur',
            'profil_id' => '1',
            'isAdmin' => false,
        ]);
        DB::table('users')->insert([
            'nom' => 'tuteurPublipress',
            'prenom' => 'tuteurPublipresse', 
            'email' => 'tuteurPublipresse@gmail.com',
            'password' => '$2y$10$85iWh73ZHRpkKlTjI5G4W.nvoNPfl29XCvnTli./a4P2IQ3dHqCBi',
            'profil_type' => 'App\Models\Tuteur',
            'profil_id' => '2',
            'isAdmin' => false,
        ]);
        DB::table('users')->insert([[
            'nom' => 'tuteurDLMSoft',
            'prenom' => 'tuteurDLMSoft', 
            'email' => 'tuteurDLMSoft@gmail.com',
            'password' => '$2y$10$85iWh73ZHRpkKlTjI5G4W.nvoNPfl29XCvnTli./a4P2IQ3dHqCBi',
            'profil_type' => 'App\Models\Tuteur',
            'profil_id' => '3',
            'isAdmin' => false,
        ], [
            'nom' => "Guinchard",
            'prenom' => "Thomas",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Apprenti',
            'profil_id' => 1,
            'email' => 'thomas@guinchard.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Midey",
            'prenom' => "Baptiste",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Apprenti',
            'profil_id' => 2,
            'email' => 'baptiste@midey.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Vieira de Sa",
            'prenom' => "Ismaël",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Apprenti',
            'profil_id' => 3,
            'email' => 'ismaël@vieiradesa.com',
            'password' => bcrypt('password')
        ],[
            'nom' => "Prigon",
            'prenom' => "Jean",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Professeur',
            'profil_id' => 1,
            'email' => 'jean@prigon.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Meliart",
            'prenom' => "Hubert",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Professeur',
            'profil_id' => 2,
            'email' => 'hubert@meliard.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Courbez",
            'prenom' => "Julian",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Rd',
            'profil_id' => 1,
            'email' => 'julian@courbez.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Shneuwly",
            'prenom' => "Olivier",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Rd',
            'profil_id' => 2,
            'email' => 'olivier@scneuwly.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Entreprise1",
            'prenom' => "Entreprise1",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Entreprise',
            'profil_id' => 1,
            'email' => 'entreprise1@gmail.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Entreprise2",
            'prenom' => "Entreprise2",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Entreprise',
            'profil_id' => 2,
            'email' => 'entreprise2@gmail.com',
            'password' => bcrypt('password')
        ], [
            'nom' => "Entreprise3",
            'prenom' => "Entreprise3",
            'isAdmin' => false,
            'profil_type' => 'App\Models\Entreprise',
            'profil_id' => 1,
            'email' => 'entreprise3@gmail.com',
            'password' => bcrypt('password')
        ]]);

    }
}
