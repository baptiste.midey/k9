<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AptitudeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aptitudes')->insert([
            'libelle' => 'C#',
        ]);
        DB::table('aptitudes')->insert([
            'libelle' => 'HTML/CSS',
        ]);
        DB::table('aptitudes')->insert([
            'libelle' => 'Symfony',
        ]);
        DB::table('aptitudes')->insert([
            'libelle' => 'Linux',
        ]);
        DB::table('aptitudes')->insert([
            'libelle' => 'Active Directory',
        ]);
    }
}
