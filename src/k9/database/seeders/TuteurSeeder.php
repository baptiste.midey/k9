<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TuteurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tuteurs')->insert([
            'telephone' => '0265478962',
            'entreprise_id' => 1,
        ]);
        DB::table('tuteurs')->insert([
            'telephone' => '0678965230',
            'entreprise_id' => 2,
        ]);
        DB::table('tuteurs')->insert([
            'telephone' => '0723654789',
            'entreprise_id' => 3,
        ]);
    }
}
