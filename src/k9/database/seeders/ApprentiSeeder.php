<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ApprentiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('apprentis')->insert([[
            'age'=> 21,
            'date_naissance' => Carbon::create('2000', '01', '01'),
            'adresse' => '5 place aux fleurs',
            'code_postal' => 39100,
            'ville' => 'Dole',
            'telephone' => '06-90-85-52-96',
            'formation_actuelle' => 'Bac général',
            'diplome' => 'Brevet',
            'specialite_lycee' => 'Physique',
            'langue_vivante_1' => 'Anglais',
            'langue_vivante_2' => 'Espagnol',
            'promotion_id' => '2',
            'option_id' => '1'
        ],[
            'age'=> 19,
            'date_naissance' => Carbon::create('2002', '09', '20'),
            'adresse' => '39 Avenue de la République',
            'code_postal' => 39000,
            'ville' => 'Lons-le-Saunier',
            'telephone' => '06-90-85-52-96',
            'formation_actuelle' => 'Bac professionnel',
            'diplome' => 'Brevet',
            'specialite_lycee' => 'SN',
            'langue_vivante_1' => 'Anglais',
            'langue_vivante_2' => 'Allemand',
            'promotion_id' => '2',
            'option_id' => '1'
        ],[
            'age'=> 23,
            'date_naissance' => Carbon::create('1998', '05', '14'),
            'adresse' => '55 Boulevard de la République',
            'code_postal' => 25000,
            'ville' => 'Besançon',
            'telephone' => '07-68-25-98-31',
            'formation_actuelle' => 'Bac général',
            'diplome' => 'Brevet',
            'specialite_lycee' => 'Physique',
            'langue_vivante_1' => 'Anglais',
            'langue_vivante_2' => 'Italien',
            'promotion_id' => '1',
            'option_id' => '2'
        ]]);
    }
}
