<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PromotionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('promotions')->insert([[
            'libelle' => 'ASI',
            'date_debut' =>  Carbon::create('2020', '09', '01'),
            'date_fin' => Carbon::create('2022', '08', '31'),
            'rd_id' => '1',
        ],[
            'libelle' => 'BTS SIO',
            'date_debut' =>  Carbon::create('2020', '09', '01'),
            'date_fin' => Carbon::create('2022', '08', '31'),
            'rd_id' => '1',
        ],[
            'libelle' => 'DCG',
            'date_debut' =>  Carbon::create('2020', '09', '01'),
            'date_fin' => Carbon::create('2022', '08', '31'),
            'rd_id' => '2',
        ]]);
    }
}
