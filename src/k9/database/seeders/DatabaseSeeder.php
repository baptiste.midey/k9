<?php

namespace Database\Seeders;

use App\Models\Aptitude;
use App\Models\Entreprise;
use Illuminate\Database\Seeder;
use App\Models\Offre;
use App\Models\Tuteur;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EntrepriseSeeder::class,
            UserSeeder::class,
            TuteurSeeder::class,
            OffreSeeder::class,
            AptitudeSeeder::class,
            AptitudeOffreSeeder::class,
            ProfesseursSeeder::class,
            RDSeeder::class,
            PromotionsSeeder::class,
            OptionsSeeder::class,
            ApprentiSeeder::class,
            FichesSeeder::class,
            EntretiensSeeder::class,
        ]);
    }
}
