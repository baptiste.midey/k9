<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkEntretiens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entretiens', function (Blueprint $table) {
            $table->unsignedBigInteger('fiche_id')->nullable();
            $table->foreign('fiche_id')->references('id')->on('fiches');
            $table->unsignedBigInteger('rd_id');
            $table->foreign('rd_id')->references('id')->on('rds');
            $table->unsignedBigInteger('apprenti_id');
            $table->foreign('apprenti_id')->references('id')->on('apprentis');
            $table->unsignedBigInteger('promotion_id');
            $table->foreign('promotion_id')->references('id')->on('apprentis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entretiens', function (Blueprint $table) {
            $table->dropForeign('fiche_id');
            $table->dropColumn('fiche_id');
            $table->dropForeign('rd_id');
            $table->dropColumn('rd_id');
            $table->dropForeign('apprenti_id');
            $table->dropColumn('apprenti_id');
            $table->dropForeign('promotion_id');
            $table->dropColumn('promotion_id');
        });
    }
}
