<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkBulletins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bulletins', function (Blueprint $table) {
            $table->unsignedBigInteger('apprenti_id')->nullable();
            $table->foreign('apprenti_id')->references('id')->on('apprentis');
            $table->unsignedBigInteger('periode_id')->nullable();
            $table->foreign('periode_id')->references('id')->on('periodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bulletins', function (Blueprint $table) {
            $table->dropForeign('apprenti_id');
            $table->dropColumn('apprenti_id');
            $table->dropForeign('periode_id');
            $table->dropColumn('periode_id');
        });
    }
}
