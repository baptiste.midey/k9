<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->unsignedBigInteger('bulletin_id')->nullable();
            $table->foreign('bulletin_id')->references('id')->on('bulletins');
            $table->unsignedBigInteger('matiere_id');
            $table->foreign('matiere_id')->references('id')->on('matieres');
            $table->unsignedBigInteger('professeur_id');
            $table->foreign('professeur_id')->references('id')->on('professeurs');
            $table->unsignedBigInteger('apprenti_id');
            $table->foreign('apprenti_id')->references('id')->on('apprentis');
            $table->unsignedBigInteger('periode_id');
            $table->foreign('periode_id')->references('id')->on('periodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->dropForeign('bulletin_id');
            $table->dropColumn('bulletin_id');
            $table->dropForeign('matiere_id');
            $table->dropColumn('matiere_id');
            $table->dropForeign('professeur_id');
            $table->dropColumn('professeur_id');
            $table->dropForeign('apprenti_id');
            $table->dropColumn('apprenti_id');
            $table->dropForeign('periode_id');
            $table->dropColumn('periode_id');
        });
    }
}
