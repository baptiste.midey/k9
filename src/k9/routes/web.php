<?php

use App\Http\Controllers\Auth\NewPasswordController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApprentiController;
use App\Http\Controllers\BackendController;
use App\Http\Controllers\OffreController;
use App\Http\Controllers\EntrepriseController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AptitudeController;
use App\Http\Controllers\BulletinController;
use App\Http\Controllers\OptionController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\PromotionEleveController;
use App\Http\Controllers\EntretienController;
use App\Http\Controllers\ProfesseurController;
use App\Http\Controllers\FicheController;
use App\Http\Controllers\MatiereController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\OffreEntreprise;
use App\Http\Controllers\PeriodeController;
use App\Http\Controllers\RdController;
use App\Http\Controllers\TuteurController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/first-password/{token}', [NewPasswordController::class, 'first'])->name('password.first');
    
// Groupe de routes avec uniquement le middleware d'authentification (connecté au site) //
Route::middleware(['auth', 'firstlogin'])->group(function () {
    Route::get('/mentions', [BackendController::class, 'mentions'])->name('mentions');
    Route::get('/', [OffreController::class, 'welcome'])->name('welcome');
    Route::get('profil', [UserController::class, 'profil'])->name('profil');
    Route::post('profil', [UserController::class, 'profilUpdate'])->name('profilUpdate');
    Route::resource('offre_entreprise', OffreEntreprise::class);
});
// Groupe de routes pour les professeurs avec middelware
Route::middleware(['auth', 'firstlogin', 'professeur'])->prefix('professeur')->group(function () {
    Route::get('/', [BackendController::class, 'profDashboard'])->name('profdashboard');
    Route::get('/matiereProfesseur', [MatiereController::class, 'matiereProfesseur'])->name('matiereProfesseur');
    Route::get('/promotionProfesseur', [PromotionController::class, 'promotionProfesseur'])->name('promotionProfesseur');
    Route::resource('notes', NoteController::class)->except(['show']);
    Route::get('notes/create/matieres/{promotion_id}', [NoteController::class, 'createMatieres'])->name('notes.create.matieres');
    Route::get('notes/create/apprentis/{promotion_id}', [NoteController::class, 'createApprentis'])->name('notes.create.apprentis');
    Route::get('notes/create/periodes/{promotion_id}', [NoteController::class, 'createPeriodes'])->name('notes.create.periodes');
});
// Groupe de routes avec le middleware d'authentification et celui d'admin du site //
Route::middleware(['auth','admin', 'firstlogin'])->group(function () {
    Route::get('fiche/pdf', [FicheController::class, 'testViewPDF'])->name('testViewPDF');
    Route::get('fiche/export/{id}', [FicheController::class, 'getPostPdf'])->name('getPostPdf');
    Route::post('/tuteur_entreprise', [OffreController::class, 'tuteurEntreprise'])->name('tuteur_entreprise');
    Route::post('/download', [BackendController::class, 'download'])->name('download');
    Route::post('/contact', [BackendController::class, 'contact'])->name('contact');
    /* Changer ces routes dans le rd */
    Route::post('/promotionPeriode', [PeriodeController::class, 'promotionPeriode'])->name('promotionPeriode');
    Route::post('/promotionApprenti', [PromotionController::class, 'promotionApprenti'])->name('promotionApprenti');
    Route::get('/bulletinPromotion', [BulletinController::class, 'bulletinPromotion'])->name('bulletinPromotion');
    Route::post('/bulletinMatiere', [BulletinController::class, 'bulletinMatiere'])->name('bulletinMatiere');
    Route::post('/promotionMatiere',[PromotionController::class, 'promotionMatiere'])->name('promotionMatiere');
    /* */
    Route::prefix('admin')->group(function () {
        Route::resource('aptitudes', AptitudeController::class);
        Route::resource('offres', OffreController::class);
        Route::resource('entreprises', EntrepriseController::class);
        Route::resource('tuteurs', TuteurController::class);
        Route::resource('users', UserController::class);
        Route::resource('options', OptionController::class);
        Route::resource('promotions', PromotionController::class);
        Route::resource('apprenti', ApprentiController::class);
        Route::resource('eleve_promotion', PromotionEleveController::class);
        Route::resource('entretiens', EntretienController::class);
        Route::resource('professeur', ProfesseurController::class);
        Route::resource('fiches', FicheController::class);
        Route::resource('rd', RdController::class);
        Route::resource('matieres',MatiereController::class);
        Route::resource('periodes',PeriodeController::class);
        // A changer dans rd
        Route::resource('bulletins',BulletinController::class);
        Route::get('/', [BackendController::class, 'dashboard'])->name('dashboard');
    });
});
require __DIR__ . '/auth.php';
